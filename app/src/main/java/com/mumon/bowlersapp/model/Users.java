package com.mumon.bowlersapp.model;

public class Users {

    private String name;
    private String password;
    private String role;
    private String birthdate;
    private String phone_no;
    private String email;
    private String status = "offline";
    private String gender;
    private String image_uri;

    public Users() {
    }

    public Users(String name, String password, String role, String email,
                 String birthdate, String phone_no, String status,
                 String gender, String image_uri) {
        this.name = name;
        this.password = password;
        this.role = role;
        this.birthdate = birthdate;
        this.phone_no = phone_no;
        this.email = email;
        this.status = status;
        this.gender = gender;
        this.image_uri = image_uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage_uri() {
        return image_uri;
    }

    public void setImage_uri(String image_uri) {
        this.image_uri = image_uri;
    }
}
