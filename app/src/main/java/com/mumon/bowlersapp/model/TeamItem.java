package com.mumon.bowlersapp.model;

public class TeamItem {
    private String imageURL, name, uid;

    public TeamItem(String imageURL, String name, String uid){
        this.imageURL = imageURL;
        this.name = name;
        this.uid = uid;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public String toString(){
        return "imgurl :" + imageURL +
                "\nname :" + name +
                "\nuid :" + uid;
    }
}
