package com.mumon.bowlersapp.model;

public class Feedback {

    private String time;
    private String day;
    private String month;
    private String year;
    private String message;

    public Feedback(){}

    public Feedback(String time, String day, String month, String year, String message) {
        this.time = time;
        this.day = day;
        this.month = month;
        this.year = year;
        this.message = message;
    }

    public String getTime() {
        return time.substring(0,5);
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
