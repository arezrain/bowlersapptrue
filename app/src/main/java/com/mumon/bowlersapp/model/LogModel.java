package com.mumon.bowlersapp.model;

public class LogModel {

    private String time;
    private String day;
    private String month;
    private String year;
    private String title;
    private String logs;

    public LogModel(){}

    public LogModel(String time, String day, String title, String logs,String month,
            String year) {
        this.time = time;
        this.day = day;
        this.title = title;
        this.logs = logs;
        this.month = month;
        this.year = year;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public String getTime() {
        return time;
    }

    public String getDay() {
        return day;
    }

    public String getTitle() {
        return title;
    }

    public String getLogs() {
        return logs;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }
}
