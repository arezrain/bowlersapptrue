package com.mumon.bowlersapp;

public class RequestItemTemp {


    private String imageURL, name, uid;

    public RequestItemTemp(String imageURL, String name, String uid) {
        this.imageURL = imageURL;
        this.name = name;
        this.uid = uid;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    @Override
    public String toString() {
        return "imgurl :" + imageURL +
                "\nname :" + name +
                "\nuid :" + uid;
    }
}


