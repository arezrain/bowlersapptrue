package com.mumon.bowlersapp.athlete;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.RequestItem;
import com.mumon.bowlersapp.adapter.TeamAdapterNew;
import com.mumon.bowlersapp.model.Users;

import java.util.ArrayList;

public class AthleteTeamListFragment extends Fragment{

    View view;
    private RecyclerView recyclerView;
    private TeamAdapterNew mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<RequestItem> requestItems = new ArrayList<>();
    ArrayList<String> requestId = new ArrayList<>();
    RelativeLayout coachBar;
    Window window;
    Toolbar toolbar;
    EditText search;

    public AthleteTeamListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.athlete_team_list_fragment, container, false);

        window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryAthlete));
        toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryAthlete));
        search = view.findViewById(R.id.athlete_team_list_search);

//        coachBar.setVisibility(View.VISIBLE);

        setUpRecyclerView(view, null);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = search.getText().toString().trim();
                if (check.isEmpty()){
                    setUpRecyclerView(view, null);
                    mAdapter.startListening();
                }
                else {
                    setUpRecyclerView(view, search.getText().toString());
                    mAdapter.startListening();
                }

            }
        });
        return view;
    }

    private void setUpRecyclerView(final View view,String keyword){

        Query query;
        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data",0);
        if (keyword!= null){
            query = FirebaseFirestore.getInstance().collection("users").whereEqualTo("team_key",sharedPreferences.getString("team_key",""))
                    .whereEqualTo("role","Athlete").whereArrayContains("keyword",keyword.toLowerCase()).orderBy("name");
        }
        else
            query = FirebaseFirestore.getInstance().collection("users").whereEqualTo("team_key",sharedPreferences.getString("team_key",""))
                    .whereEqualTo("role","Athlete").orderBy("name");

        FirestoreRecyclerOptions<Users> recyclerOptions = new FirestoreRecyclerOptions.Builder<Users>()
                .setQuery(query, Users.class)
                .build();

        mAdapter = new TeamAdapterNew(recyclerOptions);

        RecyclerView recyclerView = view.findViewById(R.id.athlete_team_recycler);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnItemClickListener(new TeamAdapterNew.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                Users user = documentSnapshot.toObject(Users.class);
                String id = documentSnapshot.getId();
                Bundle bundle = new Bundle();
                bundle.putString("name",user.getName());
                bundle.putString("id",id);
                bundle.putString("birthdate",user.getBirthdate());
                bundle.putString("email",user.getEmail());
                bundle.putString("phone_no",user.getPhone_no());
                bundle.putString("profile_uri",user.getImage_uri());
//                Navigation.findNavController(view).navigate(R.id.action_fragmentAthleteList_to_coachAthleteProfileFragment, bundle);
                Toast.makeText(getActivity(), "ID : "+id,Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.startListening();
    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}
