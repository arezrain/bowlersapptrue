package com.mumon.bowlersapp.athlete;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavHostController;
import androidx.navigation.Navigation;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.mumon.bowlersapp.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AthleteAddTrainingDataFragment extends Fragment {

    private Button saveButton, helpButton;
    private CardView shortCard, mediumCard, longCard, runningCard, driveCard;
    private EditText shortScore, shortShot, mediumScore, mediumShot,
            longScore, longShot, runningScore, runningShot, driveScore, driveShot;
    private ImageView shortWarning, mediumWarning, longWarning, runningWarning, driveWarning;
    private CheckBox shortCheckbox, mediumCheckbox, longCheckbox, runningCheckbox, driveCheckbox, allData;
    private Date date = new Date();
    private String dateText = (String) android.text.format.DateFormat.format("yyyy-MMM-dd",date);
    private String day = dateText.substring(9,11);
    private String month = dateText.substring(5,8);
    private String year = dateText.substring(0,4);
    private String trainingId = day+month+year;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private DocumentReference documentReference;

    private int weeklyScore = 0, weeklyShot = 0,
            monthlyScore = 0, monthlyShot = 0,
            yearlyScore = 0, yearlyShot = 0;

    private int totShortScore = 0, totShortShot = 0,
            totMediumScore = 0, totMediumShot = 0,
            totLongScore = 0, totLongShot = 0,
            totRunningScore = 0, totRunningShot = 0,
            totDriveScore = 0, totalDriveShot = 0;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.athlete_add_training_data_fragment, container, false);

        saveButton = view.findViewById(R.id.add_training_save_button);

        //EditText Widget
        shortScore = (EditText) view.findViewById(R.id.short_score_input);
        shortShot = (EditText) view.findViewById(R.id.short_shot_input);
        mediumScore = (EditText) view.findViewById(R.id.medium_score_input);
        mediumShot = (EditText) view.findViewById(R.id.medium_shot_input);
        longScore = (EditText) view.findViewById(R.id.long_score_input);
        longShot = (EditText) view.findViewById(R.id.long_shot_input);
        runningScore = (EditText) view.findViewById(R.id.running_score_input);
        runningShot = (EditText) view.findViewById(R.id.running_shot_input);
        driveScore = (EditText) view.findViewById(R.id.drive_score_input);
        driveShot = (EditText) view.findViewById(R.id.drive_shot_input);

        //Card Widget
        shortCard = (CardView) view.findViewById(R.id.short_card);
        mediumCard = (CardView) view.findViewById(R.id.medium_card);
        longCard = (CardView) view.findViewById(R.id.long_card);
        runningCard = (CardView) view.findViewById(R.id.running_card);
        driveCard = (CardView) view.findViewById(R.id.drive_card);

        //Checkbox Widget
        shortCheckbox = (CheckBox) view.findViewById(R.id.short_checkbox);
        mediumCheckbox = (CheckBox) view.findViewById(R.id.medium_checkbox);
        longCheckbox = (CheckBox) view.findViewById(R.id.long_checkbox);
        runningCheckbox = (CheckBox) view.findViewById(R.id.running_checkbox);
        driveCheckbox = (CheckBox) view.findViewById(R.id.drive_checkbox);

        //Warning
        shortWarning = (ImageView) view.findViewById(R.id.short_warning);
        mediumWarning = (ImageView) view.findViewById(R.id.medium_warning);
        longWarning = (ImageView) view.findViewById(R.id.long_warning);
        runningWarning = (ImageView) view.findViewById(R.id.running_warning);
        driveWarning = (ImageView) view.findViewById(R.id.drive_warning);

        helpButton = view.findViewById(R.id.help_button);

        allData= view.findViewById(R.id.All_checkbox);

        final float shortCardSizeX = shortCard.getScaleX();
        final float shortCardSizeY = shortCard.getScaleY();

        allData.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (allData.isChecked()){
                    shortCheckbox.setChecked(true);
                    mediumCheckbox.setChecked(true);
                    longCheckbox.setChecked(true);
                    runningCheckbox.setChecked(true);
                    driveCheckbox.setChecked(true);

                    shortCheckbox.setEnabled(false);
                    mediumCheckbox.setEnabled(false);
                    longCheckbox.setEnabled(false);
                    runningCheckbox.setEnabled(false);
                    driveCheckbox.setEnabled(false);
                }
                else {

                    shortCheckbox.setEnabled(true);
                    mediumCheckbox.setEnabled(true);
                    longCheckbox.setEnabled(true);
                    runningCheckbox.setEnabled(true);
                    driveCheckbox.setEnabled(true);

                }

            }
        });

        shortCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (shortCheckbox.isChecked() || mediumCheckbox.isChecked() || longCheckbox.isChecked() || runningCheckbox.isChecked() ||
                        driveCheckbox.isChecked())
                    saveButton.setVisibility(View.VISIBLE);
                else
                    saveButton.setVisibility(View.GONE);

                if (b) {
                    shortCard.setScaleX(0);
                    shortCard.setScaleY(0);
                    shortCard.setVisibility(View.VISIBLE);
                    shortCard.animate().scaleX(shortCardSizeX).scaleY(shortCardSizeY);
                }
                else{
                    shortCard.animate().scaleX(0).scaleY(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            shortCard.setVisibility(View.GONE);
                        }
                    });
                    shortWarning.setVisibility(View.GONE);
                }
            }
        });

        mediumCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (shortCheckbox.isChecked() || mediumCheckbox.isChecked() || longCheckbox.isChecked() || runningCheckbox.isChecked() ||
                        driveCheckbox.isChecked())
                    saveButton.setVisibility(View.VISIBLE);
                else
                    saveButton.setVisibility(View.GONE);

                if (b) {
                    mediumCard.setScaleX(0);
                    mediumCard.setScaleY(0);
                    mediumCard.setVisibility(View.VISIBLE);
                    mediumCard.animate().scaleX(shortCardSizeX).scaleY(shortCardSizeY);
                }
                else{
                    mediumCard.animate().scaleX(0).scaleY(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            mediumCard.setVisibility(View.GONE);
                        }
                    });
                    mediumWarning.setVisibility(View.GONE);
                }
            }
        });

        longCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (shortCheckbox.isChecked() || mediumCheckbox.isChecked() || longCheckbox.isChecked() || runningCheckbox.isChecked() ||
                        driveCheckbox.isChecked())
                    saveButton.setVisibility(View.VISIBLE);
                else
                    saveButton.setVisibility(View.GONE);

                if (b) {
                    longCard.setScaleX(0);
                    longCard.setScaleY(0);
                    longCard.setVisibility(View.VISIBLE);
                    longCard.animate().scaleX(shortCardSizeX).scaleY(shortCardSizeY);
                }
                else{
                    longCard.animate().scaleX(0).scaleY(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            longCard.setVisibility(View.GONE);
                        }
                    });
                    longWarning.setVisibility(View.GONE);
                }
            }
        });

        runningCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (shortCheckbox.isChecked() || mediumCheckbox.isChecked() || longCheckbox.isChecked() || runningCheckbox.isChecked() ||
                        driveCheckbox.isChecked())
                    saveButton.setVisibility(View.VISIBLE);
                else
                    saveButton.setVisibility(View.GONE);

                if (b) {
                    runningCard.setScaleX(0);
                    runningCard.setScaleY(0);
                    runningCard.setVisibility(View.VISIBLE);
                    runningCard.animate().scaleX(shortCardSizeX).scaleY(shortCardSizeY);
                }
                else{
                    runningCard.animate().scaleX(0).scaleY(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            runningCard.setVisibility(View.GONE);
                        }
                    });
                    runningWarning.setVisibility(View.GONE);
                }
            }
        });

        driveCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (shortCheckbox.isChecked() || mediumCheckbox.isChecked() || longCheckbox.isChecked() || runningCheckbox.isChecked() ||
                        driveCheckbox.isChecked())
                    saveButton.setVisibility(View.VISIBLE);
                else
                    saveButton.setVisibility(View.GONE);

                if (b) {
                    driveCard.setScaleX(0);
                    driveCard.setScaleY(0);
                    driveCard.setVisibility(View.VISIBLE);
                    driveCard.animate().scaleX(shortCardSizeX).scaleY(shortCardSizeY);
                }
                else{
                    driveCard.animate().scaleX(0).scaleY(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            driveCard.setVisibility(View.GONE);
                        }
                    });
                    driveWarning.setVisibility(View.GONE);
                }
            }
        });




        //check shot input
        shortShot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = shortShot.getText().toString();
                String score = shortScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(shortShot.getText().toString()) < Integer.valueOf(shortScore.getText().toString()) ||
                            (Integer.valueOf(shortScore.getText().toString().trim()) == 0 && Integer.valueOf(shortShot.getText().toString().trim()) == 0))
                        shortWarning.setVisibility(View.VISIBLE);
                    else
                        shortWarning.setVisibility(View.GONE);
                }

            }
        });

        mediumShot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = mediumShot.getText().toString();
                String score = mediumScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(mediumShot.getText().toString()) < Integer.valueOf(mediumScore.getText().toString()) ||
                            (Integer.valueOf(mediumScore.getText().toString().trim()) == 0 && Integer.valueOf(mediumShot.getText().toString().trim()) == 0))
                        mediumWarning.setVisibility(View.VISIBLE);
                    else
                        mediumWarning.setVisibility(View.GONE);
                }
            }
        });

        longShot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = longShot.getText().toString();
                String score = longScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(longShot.getText().toString()) < Integer.valueOf(longScore.getText().toString()) ||
                            (Integer.valueOf(longScore.getText().toString().trim()) == 0 && Integer.valueOf(longShot.getText().toString().trim()) == 0))
                        longWarning.setVisibility(View.VISIBLE);
                    else
                        longWarning.setVisibility(View.GONE);
                }
            }
        });

        runningShot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = runningShot.getText().toString();
                String score = runningScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(runningShot.getText().toString()) < Integer.valueOf(runningScore.getText().toString()) ||
                            (Integer.valueOf(runningScore.getText().toString().trim()) == 0 && Integer.valueOf(runningShot.getText().toString().trim()) == 0))
                        runningWarning.setVisibility(View.VISIBLE);
                    else
                        runningWarning.setVisibility(View.GONE);
                }
            }
        });

        driveShot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = driveShot.getText().toString();
                String score = driveScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(driveShot.getText().toString()) < Integer.valueOf(driveScore.getText().toString()) ||
                            (Integer.valueOf(driveScore.getText().toString().trim()) == 0 && Integer.valueOf(driveShot.getText().toString().trim()) == 0))
                        driveWarning.setVisibility(View.VISIBLE);
                    else
                        driveWarning.setVisibility(View.GONE);
                }
            }
        });




        //check score input
        shortScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = shortShot.getText().toString();
                String score = shortScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty() ) {
                    if (Integer.valueOf(shortShot.getText().toString()) < Integer.valueOf(shortScore.getText().toString()) ||
                            (Integer.valueOf(shortScore.getText().toString().trim()) == 0 && Integer.valueOf(shortShot.getText().toString().trim()) == 0))
                        shortWarning.setVisibility(View.VISIBLE);
                    else
                        shortWarning.setVisibility(View.GONE);
                }
            }
        });

        mediumScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = mediumShot.getText().toString();
                String score = mediumScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(mediumShot.getText().toString()) < Integer.valueOf(mediumScore.getText().toString()) ||
                            (Integer.valueOf(mediumScore.getText().toString().trim()) == 0 && Integer.valueOf(mediumShot.getText().toString().trim()) == 0))
                        mediumWarning.setVisibility(View.VISIBLE);
                    else
                        mediumWarning.setVisibility(View.GONE);
                }
            }
        });

        longScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = longShot.getText().toString();
                String score = longScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(longShot.getText().toString()) < Integer.valueOf(longScore.getText().toString()) ||
                            (Integer.valueOf(longScore.getText().toString().trim()) == 0 && Integer.valueOf(longShot.getText().toString().trim()) == 0))
                        longWarning.setVisibility(View.VISIBLE);
                    else
                        longWarning.setVisibility(View.GONE);
                }
            }
        });

        runningScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = runningShot.getText().toString();
                String score = runningScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(runningShot.getText().toString()) < Integer.valueOf(runningScore.getText().toString()) ||
                            (Integer.valueOf(runningScore.getText().toString().trim()) == 0 && Integer.valueOf(runningShot.getText().toString().trim()) == 0))
                        runningWarning.setVisibility(View.VISIBLE);
                    else
                        runningWarning.setVisibility(View.GONE);
                }
            }
        });

        driveScore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String shot = driveShot.getText().toString();
                String score = driveScore.getText().toString();

                if (!shot.isEmpty() && !score.isEmpty()) {
                    if (Integer.valueOf(driveShot.getText().toString()) < Integer.valueOf(driveScore.getText().toString()) ||
                            (Integer.valueOf(driveScore.getText().toString().trim()) == 0 && Integer.valueOf(driveShot.getText().toString().trim()) == 0))
                        driveWarning.setVisibility(View.VISIBLE);
                    else
                        driveWarning.setVisibility(View.GONE);
                }
            }
        });





        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean save = getInvalidInput();

                if (save){
                    documentReference = firebaseFirestore.collection("users").document(user.getUid()).collection("training_data")
                            .document(trainingId);

                    documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()){
                                DocumentSnapshot documentSnapshot = task.getResult();
                                if (documentSnapshot.exists()){
                                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which){
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    replaceData();
                                                    Toast.makeText(getActivity(),"Replacing successful",Toast.LENGTH_SHORT ).show();
                                                    getActivity().onBackPressed();
                                                    break;

                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    break;
                                            }
                                        }
                                    };

                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("Your data for today already exist, do you want to replace?").setPositiveButton("Yes", dialogClickListener)
                                            .setNegativeButton("No", dialogClickListener).show();
                                }
                                else {
                                    saveTrainingData();
                                    Toast.makeText(getActivity(),"Saved!",Toast.LENGTH_SHORT ).show();
                                    getActivity().onBackPressed();
                                }
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(getActivity(),"Please make sure the data meet requirement",Toast.LENGTH_SHORT).show();
                }
            }
        });


        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final android.app.AlertDialog helpDialog;
                ViewGroup viewGroup = view.findViewById(android.R.id.content);
                View emailView = LayoutInflater.from(getContext()).inflate(R.layout.add_help, viewGroup, false);

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());

                builder.setView(emailView);
                builder.setCancelable(true);

                helpDialog = builder.create();
                helpDialog.show();
            }
        });

        return view;
    }

     private boolean getInvalidInput(){
        boolean save;
        boolean shortSave = true;
        boolean mediumSave = true;
        boolean longSave = true;
        boolean runningSave = true;
        boolean driveSave = true;

        if (shortCard.getVisibility() == View.VISIBLE){
            if (shortScore.getText().toString().isEmpty() || shortShot.getText().toString().isEmpty()){
                Toast.makeText(getActivity(),"please fill all the data choosen!",Toast.LENGTH_SHORT).show();
                shortSave = false;
            }
            else if (shortWarning.getVisibility() == View.VISIBLE){
                Toast.makeText(getActivity(),"total shot cannot be less than score!",Toast.LENGTH_SHORT).show();
                shortSave = false;
            }
            else shortSave = true;
        }

        if (mediumCard.getVisibility() == View.VISIBLE){
            if (mediumScore.getText().toString().isEmpty() || mediumShot.getText().toString().isEmpty()){
                Toast.makeText(getActivity(),"please fill all the data choosen!",Toast.LENGTH_SHORT).show();
                mediumSave = false;
            }
            else if (mediumWarning.getVisibility() == View.VISIBLE){
                Toast.makeText(getActivity(),"total shot cannot be less than score!",Toast.LENGTH_SHORT).show();
                mediumSave = false;
            }
            else mediumSave = true;
        }

        if (longCard.getVisibility() == View.VISIBLE){
            if (longScore.getText().toString().isEmpty() || longShot.getText().toString().isEmpty()){
                Toast.makeText(getActivity(),"please fill all the data choosen!",Toast.LENGTH_SHORT).show();
                longSave = false;
            }
            else if (longWarning.getVisibility() == View.VISIBLE){
                Toast.makeText(getActivity(),"total shot cannot be less than score!",Toast.LENGTH_SHORT).show();
                longSave = false;
            }
            else longSave = true;
        }

        if (runningCard.getVisibility() == View.VISIBLE){
            if (runningScore.getText().toString().isEmpty() || runningShot.getText().toString().isEmpty()){
                Toast.makeText(getActivity(),"please fill all the data choosen!",Toast.LENGTH_SHORT).show();
                runningSave = false;
            }
            else if (runningWarning.getVisibility() == View.VISIBLE){
                Toast.makeText(getActivity(),"total shot cannot be less than score!",Toast.LENGTH_SHORT).show();
                runningSave = false;
            }
            else runningSave = true;
        }

        if (driveCard.getVisibility() == View.VISIBLE){
            if (driveScore.getText().toString().isEmpty() || driveShot.getText().toString().isEmpty()){
                Toast.makeText(getActivity(),"please fill all the data choosen!",Toast.LENGTH_SHORT).show();
                driveSave = false;
            }
            else if (driveWarning.getVisibility() == View.VISIBLE){
                Toast.makeText(getActivity(),"total shot cannot be less than score!",Toast.LENGTH_SHORT).show();
                driveSave = false;
            }
            else driveSave = true;
        }

        if (!shortSave || !mediumSave || !longSave || !runningSave || !driveSave)
            save = false;
        else
            save = true;

        return save ;
    }

    private void saveTrainingData(){

        String week ="";



        if (shortCard.getVisibility() == View.VISIBLE && !shortShot.getText().toString().isEmpty() &&
                !shortScore.getText().toString().isEmpty()){
            totShortScore = Integer.valueOf(shortScore.getText().toString().trim());
            totShortShot = Integer.valueOf(shortShot.getText().toString().trim());
        }

        if (mediumCard.getVisibility() == View.VISIBLE && !mediumShot.getText().toString().isEmpty() &&
                !mediumScore.getText().toString().isEmpty()){
            totMediumScore = Integer.valueOf(mediumScore.getText().toString().trim());
            totMediumShot = Integer.valueOf(mediumShot.getText().toString().trim());
        }

        if (longCard.getVisibility() == View.VISIBLE && !longShot.getText().toString().isEmpty() &&
                !longScore.getText().toString().isEmpty()){
            totLongScore = Integer.valueOf(longScore.getText().toString().trim());
            totLongShot = Integer.valueOf(longShot.getText().toString().trim());
        }

        if (runningCard.getVisibility() == View.VISIBLE && !runningShot.getText().toString().isEmpty() &&
                !runningScore.getText().toString().isEmpty()){
            totRunningScore = Integer.valueOf(runningScore.getText().toString().trim());
            totRunningShot = Integer.valueOf(runningShot.getText().toString().trim());
        }

        if (driveCard.getVisibility() == View.VISIBLE && !driveShot.getText().toString().isEmpty() &&
                !driveScore.getText().toString().isEmpty()){
            totDriveScore = Integer.valueOf(driveScore.getText().toString().trim());
            totalDriveShot = Integer.valueOf(driveShot.getText().toString().trim());
        }


        //check which week
        if (Integer.valueOf(day) >= 1 && Integer.valueOf(day) <= 7){
            week = "week1";
        }

        else if (Integer.valueOf(day) >= 8 && Integer.valueOf(day) <= 14){
            week = "week2";
        }

        else if (Integer.valueOf(day) >= 15 && Integer.valueOf(day) <= 21){
            week = "week3";
        }

        else if (Integer.valueOf(day) >= 22 && Integer.valueOf(day) <= 28){
            week = "week4";
        }

        else if (Integer.valueOf(day) >= 29 && Integer.valueOf(day) <= 31){
            week = "week5";
        }

        System.out.println(week+month+year);

        final String finalWeek = week;
        firebaseFirestore.document("users/"+user.getUid()+
                "/training_data_accuracy_weekly/"+week+month+year).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    weeklyScore = documentSnapshot.getLong("score").intValue();
                    weeklyShot = documentSnapshot.getLong("shot").intValue();
                    System.out.println("Weekly Exist "+weeklyScore+","+weeklyShot);
                    weeklyScore += totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                    weeklyShot += totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;
                    System.out.println("Weekly Exist "+weeklyScore+","+weeklyShot);
                }
                else {
                    weeklyScore += totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                    weeklyShot += totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;
                }

                Map <String,Object> trainingDataWeekly = new HashMap<>();

                trainingDataWeekly.put("score",weeklyScore);
                trainingDataWeekly.put("shot",weeklyShot);
                trainingDataWeekly.put("week",finalWeek);
                trainingDataWeekly.put("month",month);
                trainingDataWeekly.put("year",year);

                firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_weekly/"+ finalWeek +month+year).set(trainingDataWeekly);
            }

        });

        firebaseFirestore.document("users/"+user.getUid()+
                "/training_data_accuracy_monthly/"+month+year).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    monthlyScore = documentSnapshot.getLong("score").intValue();
                    monthlyShot = documentSnapshot.getLong("shot").intValue();
                    System.out.println("Monthly Exist "+monthlyScore+","+monthlyShot);
                    monthlyScore += totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                    monthlyShot += totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;
                    System.out.println("Monthly Exist "+monthlyScore+","+monthlyShot);
                }
                else{
                    monthlyScore += totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                    monthlyShot += totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;
                }

                Map <String,Object> trainingDataMonthly = new HashMap<>();

                int no = 0;

                switch (month) {
                    case "Jan":
                        no = 1;
                        break;
                    case "Feb":
                        no = 2;
                        break;
                    case "Mar":
                        no = 3;
                        break;
                    case "Apr":
                        no = 4;
                        break;
                    case "May":
                        no = 5;
                        break;
                    case "Jun":
                        no = 6;
                        break;
                    case "Jul":
                        no = 7;
                        break;
                    case "Aug":
                        no = 8;
                        break;
                    case "Sep":
                        no = 9;
                        break;
                    case "Oct":
                        no = 10;
                        break;
                    case "Nov":
                        no = 11;
                        break;
                    case "Dec":
                        no = 12;
                        break;
                }

                trainingDataMonthly.put("no",no);
                trainingDataMonthly.put("score",monthlyScore);
                trainingDataMonthly.put("shot",monthlyShot);
                trainingDataMonthly.put("month",month);
                trainingDataMonthly.put("year",year);

                firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_monthly/"+month+year).set(trainingDataMonthly);
            }
        });

        firebaseFirestore.document("users/"+user.getUid()+
                "/training_data_accuracy_yearly/"+year).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                if (documentSnapshot.exists()){
                    yearlyScore = documentSnapshot.getLong("score").intValue();
                    yearlyShot = documentSnapshot.getLong("shot").intValue();
                    System.out.println("Yearly Exist "+yearlyScore+","+yearlyShot);
                    yearlyScore += totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                    yearlyShot += totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;
                    System.out.println("Yearly Exist "+yearlyScore+","+yearlyShot);

                }
                else {
                    yearlyScore += totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                    yearlyShot += totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;
                }

                Map <String,Object> trainingDataYearly = new HashMap<>();

                trainingDataYearly.put("score",yearlyScore);
                trainingDataYearly.put("shot",yearlyShot);
                trainingDataYearly.put("year",year);

                firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_yearly/"+year).set(trainingDataYearly);
            }
        });

        System.out.println("weekly "+ weeklyScore+" "+weeklyShot+" monthly "+ monthlyScore+" "+monthlyShot+ " yearly "+ yearlyScore+ " "+yearlyShot);

        Map <String,Object> trainingData = new HashMap();

        trainingData.put("short_score",totShortScore);
        trainingData.put("short_shot",totShortShot);

        trainingData.put("medium_score",totMediumScore);
        trainingData.put("medium_shot",totMediumShot);

        trainingData.put("long_score",totLongScore);
        trainingData.put("long_shot",totLongShot);

        trainingData.put("running_score",totRunningScore);
        trainingData.put("running_shot",totRunningShot);

        trainingData.put("drive_score",totDriveScore);
        trainingData.put("drive_shot",totalDriveShot);

        int totScore = totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
        int totShot = totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;

        trainingData.put("score",totScore);
        trainingData.put("shot", totShot);

        Calendar calendar = Calendar.getInstance();
        trainingData.put("month_no",calendar.get(Calendar.MONTH));

        trainingData.put("day",day);
        trainingData.put("month",month);
        trainingData.put("year",year);






//
        firebaseFirestore.document("users/"+user.getUid()+"/training_data/"+trainingId).set(trainingData);

    }

    public void replaceData(){
        Toast.makeText(getActivity(),"Replacing successful",Toast.LENGTH_SHORT ).show();

        if (shortCard.getVisibility() == View.VISIBLE && !shortShot.getText().toString().isEmpty() &&
                !shortScore.getText().toString().isEmpty()){
            totShortScore = Integer.valueOf(shortScore.getText().toString().trim());
            totShortShot = Integer.valueOf(shortShot.getText().toString().trim());
        }

        if (mediumCard.getVisibility() == View.VISIBLE && !mediumShot.getText().toString().isEmpty() &&
                !mediumScore.getText().toString().isEmpty()){
            totMediumScore = Integer.valueOf(mediumScore.getText().toString().trim());
            totMediumShot = Integer.valueOf(mediumShot.getText().toString().trim());
        }

        if (longCard.getVisibility() == View.VISIBLE && !longShot.getText().toString().isEmpty() &&
                !longScore.getText().toString().isEmpty()){
            totLongScore = Integer.valueOf(longScore.getText().toString().trim());
            totLongShot = Integer.valueOf(longShot.getText().toString().trim());
        }

        if (runningCard.getVisibility() == View.VISIBLE && !runningShot.getText().toString().isEmpty() &&
                !runningScore.getText().toString().isEmpty()){
            totRunningScore = Integer.valueOf(runningScore.getText().toString().trim());
            totRunningShot = Integer.valueOf(runningShot.getText().toString().trim());
        }

        if (driveCard.getVisibility() == View.VISIBLE && !driveShot.getText().toString().isEmpty() &&
                !driveScore.getText().toString().isEmpty()){
            totDriveScore = Integer.valueOf(driveScore.getText().toString().trim());
            totalDriveShot = Integer.valueOf(driveShot.getText().toString().trim());
        }

        firebaseFirestore.document("users/"+user.getUid()+"/training_data/"+trainingId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    final int oldScore = documentSnapshot.getLong("score").intValue();
                    final int oldShot = documentSnapshot.getLong("shot").intValue();
                    String week = "";

                    if (Integer.valueOf(day) >= 1 && Integer.valueOf(day) <= 7){
                        week = "week1";
                    }

                    else if (Integer.valueOf(day) >= 8 && Integer.valueOf(day) <= 14){
                        week = "week2";
                    }

                    else if (Integer.valueOf(day) >= 15 && Integer.valueOf(day) <= 21){
                        week = "week3";
                    }

                    else if (Integer.valueOf(day) >= 22 && Integer.valueOf(day) <= 28){
                        week = "week4";
                    }

                    else if (Integer.valueOf(day) >= 29 && Integer.valueOf(day) <= 31){
                        week = "week5";
                    }

                    final String finalWeek = week;
                    firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_weekly/"+week+month+year).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()){
                                int weekScore = documentSnapshot.getLong("score").intValue();
                                int weekShot = documentSnapshot.getLong("shot").intValue();

                                weekScore = weekScore - oldScore + totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                                weekShot = weekShot - oldShot + totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;

                                Map<String,Object> newData = new HashMap<>();
                                newData.put("score",weekScore);
                                newData.put("shot", weekShot);
                                newData.put("week",finalWeek);
                                newData.put("month", month);
                                newData.put("year",year);

                                System.out.println(newData);
                                firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_weekly/"+ finalWeek+month+year).set(newData);
                            }
                        }
                    });


                    firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_monthly/"+month+year).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()){
                                int monthScore = documentSnapshot.getLong("score").intValue();
                                int monthShot = documentSnapshot.getLong("shot").intValue();

                                monthScore = monthScore - oldScore + totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                                monthShot = monthShot - oldShot + totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;


                                int no = 0;

                                switch (month) {
                                    case "Jan":
                                        no = 1;
                                        break;
                                    case "Feb":
                                        no = 2;
                                        break;
                                    case "Mar":
                                        no = 3;
                                        break;
                                    case "Apr":
                                        no = 4;
                                        break;
                                    case "May":
                                        no = 5;
                                        break;
                                    case "Jun":
                                        no = 6;
                                        break;
                                    case "Jul":
                                        no = 7;
                                        break;
                                    case "Aug":
                                        no = 8;
                                        break;
                                    case "Sep":
                                        no = 9;
                                        break;
                                    case "Oct":
                                        no = 10;
                                        break;
                                    case "Nov":
                                        no = 11;
                                        break;
                                    case "Dec":
                                        no = 12;
                                        break;
                                }

                                Map<String,Object> newData = new HashMap<>();

                                newData.put("score",monthScore);
                                newData.put("shot",monthShot);
                                newData.put("no",no);
                                newData.put("month", month);
                                newData.put("year", year);

                                System.out.println(newData);
                                firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_monthly/"+month+year).set(newData);
                            }
                        }
                    });


                    firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_yearly/"+year).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()){
                                int yearScore = documentSnapshot.getLong("score").intValue();
                                int yearShot = documentSnapshot.getLong("shot").intValue();

                                yearScore = yearScore - oldScore + totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                                yearShot = yearShot - oldShot + totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;

                                Map<String,Object> newData = new HashMap<>();

                                newData.put("score",yearScore);
                                newData.put("shot",yearShot);
                                newData.put("year", year);

                                System.out.println(newData);
                                System.out.println(oldScore+" "+yearScore);
                                firebaseFirestore.document("users/"+user.getUid()+"/training_data_accuracy_yearly/"+year).set(newData);
                            }
                        }
                    });

                }


                Map <String,Object> trainingData = new HashMap();

                trainingData.put("short_score",totShortScore);
                trainingData.put("short_shot",totShortShot);

                trainingData.put("medium_score",totMediumScore);
                trainingData.put("medium_shot",totMediumShot);

                trainingData.put("long_score",totLongScore);
                trainingData.put("long_shot",totLongShot);

                trainingData.put("running_score",totRunningScore);
                trainingData.put("running_shot",totRunningShot);

                trainingData.put("drive_score",totDriveScore);
                trainingData.put("drive_shot",totalDriveShot);

                int totScore = totShortScore + totMediumScore + totLongScore + totRunningScore + totDriveScore;
                int totShot = totShortShot + totMediumShot + totLongShot + totRunningShot + totalDriveShot;

                trainingData.put("score",totScore);
                trainingData.put("shot", totShot);

                Calendar calendar = Calendar.getInstance();
                trainingData.put("month_no",calendar.get(Calendar.MONTH));

                trainingData.put("day",day);
                trainingData.put("month",month);
                trainingData.put("year",year);

                firebaseFirestore.document("users/"+user.getUid()+"/training_data/"+trainingId).set(trainingData);

            }
        });



    }

}