package com.mumon.bowlersapp.athlete;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mumon.bowlersapp.MainActivity;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.coach.CoachMainActivity;
import com.mumon.bowlersapp.model.Users;

public class NewAthleteActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public NavController navController;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    Toolbar toolbar;

    FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();



    TextView name, email;
    ImageView profilePic;
    int currentPage = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_athlete);

        setupNavigation();
        fetchUserData();
    }

    private void fetchUserData() {
        FirebaseFirestore.getInstance().document("users/"+FirebaseAuth.getInstance().getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Users users = new Users();
                        users = documentSnapshot.toObject(Users.class);

                        name.setText(users.getName());
                        email.setText(users.getEmail());
                        Glide.with(NewAthleteActivity.this)
                                .load(users.getImage_uri())
                                .circleCrop()
                                .into(profilePic);

                    }
                });
    }

    private void setupNavigation() {

        toolbar = findViewById(R.id.toolbar);

//        toolbar.setVisibility(View.INVISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerLayout = findViewById(R.id.athlete_drawer_layout);

        navigationView = findViewById(R.id.navigationView);

        navController = Navigation.findNavController(this, R.id.athlete_nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout);

        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        name = headerView.findViewById(R.id.athlete_drawer_name);
        email = headerView.findViewById(R.id.athlete_drawer_email);
        profilePic = headerView.findViewById(R.id.athlete_drawer_profile_pic);

    }

    @Override
    protected void onStart() {
        super.onStart();

        firebaseFirestore.document("users/"+user.getUid()).update("status","online");
    }

    @Override
    protected void onStop() {
        super.onStop();

        firebaseFirestore.document("users/"+user.getUid()).update("status","offline");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        firebaseFirestore.document("users/"+user.getUid()).update("status","online");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        firebaseFirestore.document("users/"+user.getUid()).update("status","offline");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        menuItem.setChecked(true);

        drawerLayout.closeDrawers();

        int id = menuItem.getItemId();



        switch (id) {

            case R.id.nav_home:
                System.out.println(currentPage+" "+R.id.athlete_home_fragment+" "+navigationView.getId());
                if (currentPage != 2131361930){
                    navController.navigate(R.id.athlete_home_fragment);
                    currentPage = R.id.coachHomeFragment;
                    break;
                }
                else {
                    break;
                }

            case R.id.nav_profile:
                System.out.println(currentPage+" "+R.id.athleteProfileFragment2+" "+navController.getCurrentDestination().getAction(getTaskId()));
                if (currentPage != 2131361931){
                    navController.navigate(R.id.athleteProfileFragment2);
                    currentPage = R.id.coachProfile;
                    break;
                }
                else {
                    break;
                }

            case R.id.nav_training_log:
                System.out.println(currentPage+" "+R.id.coachTrainingLog+" "+navController.getCurrentDestination().getAction(getTaskId()));
                if (currentPage != 2131361932){
                    navController.navigate(R.id.athleteTrainingNote);
                    currentPage = R.id.athleteTrainingNote;
                    break;
                }
                else {
                    break;
                }

            case R.id.coach_logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(NewAthleteActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return true;

    }

}
