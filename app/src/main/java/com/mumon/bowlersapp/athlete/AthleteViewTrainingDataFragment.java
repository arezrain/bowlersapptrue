package com.mumon.bowlersapp.athlete;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.model.GradientColor;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.adapter.FeedbackAdapter;
import com.mumon.bowlersapp.adapter.FeedbackAdapterAthlete;
import com.mumon.bowlersapp.geofence.GeofenceSetting;
import com.mumon.bowlersapp.model.Feedback;
import com.mumon.bowlersapp.valueFormatter.DailyValueFormatter;
import com.mumon.bowlersapp.valueFormatter.MonthlyValueFormatter;
import com.mumon.bowlersapp.valueFormatter.WeeklyValueFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AthleteViewTrainingDataFragment extends Fragment  /*implements DatePickerDialog.OnDateSetListener*/ {

    TextView athleteName, athleteEmail, athleteBirthdate, athletePhone, statusText, spinnerText, radarDateButtonText, radarSpinnerButtonText;
    RelativeLayout coachBar;
    ImageView profileImage, onlineIcon, offlineIcon, commentSent;
    RadarChart radarChart;
    BarChart barChart;
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    LinearLayout spinnerButton, radarDateButton, radarSpinnerButton;
    CardView radarContainer, barContainer;
    EditText comment;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    Spinner yearSpinner, yearSpinnerBar;
    Button whatsappButton, callButton;
    FloatingActionButton addData;

    private double latitude;
    private double longitude;
    double tempLatitude;
    double tempLongitude;
    private double oldLatitude;
    private double oldLongitude;

    private double dbLatitude;
    private double dbLongitude;
    private float dbRadius;

    LocationManager locationManager;


    private Date date = new Date();
    private String dateText = (String) android.text.format.DateFormat.format("yyyy-MMM-dd", date);
    private String day = dateText.substring(9, 11);
    private String month = dateText.substring(5, 8);
    private String year = dateText.substring(0, 4);
    private String monthDateText = (String) android.text.format.DateFormat.format("MM", date);
    private String trainingId = day + month + year;
    Window window;
    Toolbar toolbar;
    private FeedbackAdapter mAdapter;
    LinearLayoutManager linearLayoutManager;
    private SpinKitView barLoading;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.athlete_view_training_data_fragment, container, false);

        window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryAthlete));
        toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryAthlete));

        radarContainer = view.findViewById(R.id.radar_container);
        barContainer = view.findViewById(R.id.bar_container);

        spinnerButton = view.findViewById(R.id.athlete_view_bar_spinner_button);
        spinnerText = view.findViewById(R.id.athlete_view_spinner_text);
        radarDateButton = view.findViewById(R.id.athlete_view_radar_date_button);
        radarSpinnerButton = view.findViewById(R.id.athlete_view_radar_chart_data_type_spinner);
        radarSpinnerButtonText = view.findViewById(R.id.athlete_view_radar_chart_data_type_spinner_text);
        yearSpinner = view.findViewById(R.id.athlete_year_spinner);
        yearSpinnerBar = view.findViewById(R.id.athlete_year_spinner_bar);

        athleteName = view.findViewById(R.id.athlete_name);
        athleteEmail = view.findViewById(R.id.athlete_email);
        radarDateButtonText = view.findViewById(R.id.athlete_view_radar_chart_date_text);


        comment = view.findViewById(R.id.comment_text);
        commentSent = view.findViewById(R.id.comment_send);

        radarChart = view.findViewById(R.id.athlete_view_radar_chart);
        barChart = view.findViewById(R.id.athlete_view_bar_chart);
        barLoading = view.findViewById(R.id.athlete_bar_loading);
        addData = view.findViewById(R.id.athlete_add_training_data_button);
//        radarChart.setEnabled(false);
//        barChart.setEnabled(false);


        fetchUserData();


        System.out.println(month+" "+year);
        generateRadarBasedDate(day, month, Integer.valueOf(year), day + " " + month);
        generateDailyBarChart(month, year);

        setUpRecyclerView(view);
        System.out.println("users/"+user.getUid()+"/feedback/"+dateText);
        ArrayList<String> yearText = new ArrayList<>();

        int setYear = Integer.valueOf(year);
        for (int i = setYear; i > setYear-15; i--){
            yearText.add(String.valueOf(i));
        }

        ArrayList<String> monthText = new ArrayList<>();
        monthText.add("January");
        monthText.add("February");
        monthText.add("March");
        monthText.add("April");
        monthText.add("May");
        monthText.add("June");
        monthText.add("July");
        monthText.add("August");
        monthText.add("September");
        monthText.add("October");
        monthText.add("November");
        monthText.add("December");
        System.out.println(monthDateText);

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(getActivity(),R.layout.black_spinner_text,yearText);
        stringArrayAdapter.setDropDownViewResource(R.layout.item_drop_down);
        yearSpinner.setAdapter(stringArrayAdapter);

        final ArrayAdapter<String> barArrayAdapter = new ArrayAdapter<>(getActivity(),R.layout.black_spinner_text,monthText);
        barArrayAdapter.setDropDownViewResource(R.layout.item_drop_down);
        yearSpinnerBar.setAdapter(barArrayAdapter);
        yearSpinnerBar.setSelection(Integer.valueOf(monthDateText)-1);

        final ArrayAdapter<String> barYearArrayAdapter = new ArrayAdapter<>(getActivity(),R.layout.black_spinner_text,yearText);
        barYearArrayAdapter.setDropDownViewResource(R.layout.item_drop_down);


        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data", 0);
        firebaseDatabase.getReference("GeofenceSetting/" + sharedPreferences.getString("team_key", "")).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            GeofenceSetting geofenceSetting = dataSnapshot.getValue(GeofenceSetting.class);
                            dbLatitude = geofenceSetting.getLatitude();
                            dbLongitude = geofenceSetting.getLongitude();
                            dbRadius = geofenceSetting.getRadius();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }
        );

        addData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(insideGeofence()){
                    Navigation.findNavController(view).navigate(R.id.action_athleteViewTrainingDataFragment_to_athleteAddTrainingDataFragment);
                }
                else {
                    Toast.makeText(getActivity(), "outside geofence", Toast.LENGTH_SHORT).show();
                    OutsideAlert(view);
                }
            }
        });

        spinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(getActivity(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.bar_type, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.bar_daily) {
                            Toast.makeText(getActivity(), "Daily", Toast.LENGTH_SHORT).show();
                            spinnerText.setText("Daily");
                            yearSpinnerBar.setAdapter(barArrayAdapter);
                            yearSpinnerBar.setSelection(Integer.valueOf(monthDateText)-1);
                            generateDailyBarChart(month, year);
                            return true;
                        } else if (item.getItemId() == R.id.bar_weekly) {
                            Toast.makeText(getActivity(), "Weekly", Toast.LENGTH_SHORT).show();
                            spinnerText.setText("Weekly");
                            yearSpinnerBar.setAdapter(barArrayAdapter);
                            yearSpinnerBar.setSelection(Integer.valueOf(monthDateText)-1);
                            generateWeeklyBarChart(month, year);
                            return true;
                        } else if (item.getItemId() == R.id.bar_monthly) {
                            Toast.makeText(getActivity(), "Monthly", Toast.LENGTH_SHORT).show();
                            spinnerText.setText("Monthly");
                            yearSpinnerBar.setAdapter(barYearArrayAdapter);
                            generateMonthlyBarChart(year);
                            return true;
                        } else
                            return false;

                    }
                });
            }
        });

        yearSpinnerBar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinnerText.getText().toString().equals("Daily")){
                    generateDailyBarChart(yearSpinnerBar.getSelectedItem().toString().substring(0,3), year);
                }
                else if (spinnerText.getText().toString().equals("Weekly")){
                    generateWeeklyBarChart(yearSpinnerBar.getSelectedItem().toString().substring(0,3), year);
                }
                else if (spinnerText.getText().toString().equals("Monthly")){{
                    generateMonthlyBarChart(yearSpinnerBar.getSelectedItem().toString());
                }}
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        radarDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DialogFragment datePicker = new DatePickerFragment();
//                datePicker.show(getParentFragmentManager(),"Date Picker");
                if (radarChart.getData() != null){
                    Calendar calendar = Calendar.getInstance();

                    if (radarSpinnerButtonText.getText().toString().equals("Daily")){

                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {


                                String passDay;
                                if (day < 10) {
                                    passDay = "0" + day;
                                } else {
                                    passDay = String.valueOf(day);
                                }

                                String[] monthName = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
                                String selectedDate = day + " " + monthName[month];

                                Calendar calendarCheck = Calendar.getInstance();
                                if (calendarCheck.get(Calendar.YEAR) == year && calendarCheck.get(Calendar.MONTH) == month && calendarCheck.get(Calendar.DAY_OF_MONTH) == day) {
                                    radarDateButtonText.setText("Today");
                                } else
                                    radarDateButtonText.setText(selectedDate);

                                System.out.println(passDay + " " + monthName[month] + " " + year + " " + selectedDate);

                                setDataBasedDate(passDay, monthName[month], year, selectedDate);

                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.show();

                    }
                    else if (radarSpinnerButtonText.getText().toString().equals("Monthly")){
                        PopupMenu popupMenu = new PopupMenu(getActivity(),view);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.month_menu,popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                String yearSpinnerText = yearSpinner.getSelectedItem().toString();
                                if (menuItem.getItemId() == R.id.jan){
                                    radarDateButtonText.setText("Jan");
                                    setDataBasedSelectedMonth(day,"Jan",yearSpinnerText,"Jan");


                                    return true;
                                }else if (menuItem.getItemId() == R.id.feb){
                                    radarDateButtonText.setText("Feb");
                                    setDataBasedSelectedMonth(day,"Feb",yearSpinnerText,"Feb");


                                    return true;
                                }else if (menuItem.getItemId() == R.id.mar){
                                    radarDateButtonText.setText("Mar");
                                    setDataBasedSelectedMonth(day,"Mar",yearSpinnerText,"Mar");


                                    return true;
                                }else if (menuItem.getItemId() == R.id.apr){
                                    radarDateButtonText.setText("Apr");
                                    setDataBasedSelectedMonth(day,"Apr",yearSpinnerText,"Apr");


                                    return true;
                                }else if (menuItem.getItemId() == R.id.may){
                                    radarDateButtonText.setText("May");
                                    setDataBasedSelectedMonth(day,"May",yearSpinnerText,"May");


                                    return true;
                                }else if (menuItem.getItemId() == R.id.jun){
                                    radarDateButtonText.setText("Jun");
                                    setDataBasedSelectedMonth(day,"Jun",yearSpinnerText,"Jun");



                                    return true;
                                }else if (menuItem.getItemId() == R.id.jul){
                                    radarDateButtonText.setText("Jul");
                                    setDataBasedSelectedMonth(day,"Jul",yearSpinnerText,"Jul");



                                    return true;
                                }else if (menuItem.getItemId() == R.id.aug){
                                    radarDateButtonText.setText("Aug");
                                    setDataBasedSelectedMonth(day,"Aug",yearSpinnerText,"Aug");



                                    return true;
                                }else if (menuItem.getItemId() == R.id.sep){
                                    radarDateButtonText.setText("Sep");
                                    setDataBasedSelectedMonth(day,"Sep",yearSpinnerText,"Sept");



                                    return true;
                                }else if (menuItem.getItemId() == R.id.oct){
                                    radarDateButtonText.setText("Oct");
                                    setDataBasedSelectedMonth(day,"Oct",yearSpinnerText,"Oct");



                                    return true;
                                }else if (menuItem.getItemId() == R.id.nov){
                                    radarDateButtonText.setText("Nov");
                                    setDataBasedSelectedMonth(day,"Nov",yearSpinnerText,"Nov");



                                    return true;
                                }else if (menuItem.getItemId() == R.id.dec){
                                    radarDateButtonText.setText("Dec");
                                    setDataBasedSelectedMonth(day,"Dec",yearSpinnerText,"Dec");



                                    return true;
                                }
                                else
                                    return false;
                            }
                        });
                    }
                }
                else {
                    Toast.makeText(getActivity(), "No data available to be selected", Toast.LENGTH_SHORT).show();
                }



            }
        });

        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Calendar calendar = Calendar.getInstance();

                String[] monthName = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

                radarDateButtonText.setText(monthName[calendar.get(Calendar.MONTH)]);

                setDataBasedSelectedMonth(day,radarDateButtonText.getText().toString(),yearSpinner.getSelectedItem().toString(),radarDateButtonText.getText().toString());
                Toast.makeText(getActivity(), radarDateButtonText.getText().toString()+" "+yearSpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        radarSpinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(getActivity(), view);
                final MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.radar_type, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.bar_daily) {
                            yearSpinner.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Daily", Toast.LENGTH_SHORT).show();
                            radarSpinnerButtonText.setText("Daily");
                            generateRadarBasedDate(day, month, Integer.valueOf(year), day + " " + month + " " + year);
                            return true;


                        }  else if (item.getItemId() == R.id.bar_monthly) {
                            yearSpinner.setVisibility(View.VISIBLE);
                            Calendar calendar = Calendar.getInstance();

                            String[] monthName = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

                            radarDateButtonText.setText(monthName[calendar.get(Calendar.MONTH)]);
                            Toast.makeText(getActivity(), "Monthly "+yearSpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                            setDataBasedMonth(day,month,year,monthName[calendar.get(Calendar.MONTH)]);
                            radarSpinnerButtonText.setText("Monthly");
                            return true;


                        } else
                            return false;

                    }
                });
            }
        });

        return view;
    }

    private void OutsideAlert(View view) {

        final android.app.AlertDialog passwordDialog;
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        Button save;
        View emailView = LayoutInflater.from(getContext()).inflate(R.layout.oops_alert_geofence, viewGroup, false);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());

        builder.setView(emailView);

        passwordDialog = builder.create();
        passwordDialog.show();

    }

    private void fetchUserData() {

        FirebaseFirestore.getInstance().document("users/"+user.getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        athleteName.setText(documentSnapshot.getString("name"));
                        athleteEmail.setText(documentSnapshot.getString("email"));
                    }
                });

    }

    private void setUpRecyclerView(View view){

        System.out.println("test comment");
        Query query = FirebaseFirestore.getInstance().collection("users").document(user.getUid())
                .collection("feedback").orderBy("year").orderBy("month").orderBy("day")
                .orderBy("time");

        FirestoreRecyclerOptions<Feedback> recyclerOptions = new FirestoreRecyclerOptions.Builder<Feedback>()
                .setQuery(query,Feedback.class)
                .build();

        mAdapter = new FeedbackAdapter(recyclerOptions);

        RecyclerView recyclerView = view.findViewById(R.id.athlete_view_feedback_recycler);
//        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.scrollToPosition(mAdapter.getItemCount()-1);
        recyclerView.setLayoutManager(linearLayoutManager);

        mAdapter.setOnItemLongClickListener(new FeedbackAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(DocumentSnapshot documentSnapshot, final int position) {
                Toast.makeText(getActivity(), "test "+documentSnapshot.getId(), Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Do you want to delete this comment?");
                builder.setCancelable(true);

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAdapter.deleteMessage(position);
                        dialogInterface.cancel();
                        Toast.makeText(getContext(), "Deleted Comment", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        mAdapter.setOnItemClickListener(new FeedbackAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
//                Toast.makeText(getActivity(), "test "+documentSnapshot.getId(), Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.setAdapter(mAdapter);



    }

    private void setDataBasedSelectedMonth(String day, String month, final String year, final String monthName) {

        Query query = FirebaseFirestore.getInstance().collection("users").document(user.getUid()).collection("training_data");

        query.whereEqualTo("month", month).whereEqualTo("year",year).orderBy("day").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            if (!task.getResult().isEmpty()){

                                float shortScore = 0, shortShot = 0;
                                float mediumScore = 0, mediumShot = 0;
                                float longScore = 0, longShot = 0;
                                float runningScore = 0, runningShot = 0;
                                float driveScore = 0, driveShot = 0;

                                for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){

                                    shortScore = shortScore + queryDocumentSnapshot.getLong("short_score").floatValue();
                                    shortShot = shortShot + queryDocumentSnapshot.getLong("short_shot").floatValue();

                                    mediumScore = mediumScore + queryDocumentSnapshot.getLong("medium_score").floatValue();
                                    mediumShot = mediumShot + queryDocumentSnapshot.getLong("medium_shot").floatValue();

                                    longScore = longScore + queryDocumentSnapshot.getLong("long_score").floatValue();
                                    longShot = longShot + queryDocumentSnapshot.getLong("long_shot").floatValue();

                                    runningScore = runningScore + queryDocumentSnapshot.getLong("running_score").floatValue();
                                    runningShot = runningShot + queryDocumentSnapshot.getLong("running_shot").floatValue();

                                    driveScore = driveScore + queryDocumentSnapshot.getLong("drive_score").floatValue();
                                    driveShot = driveShot + queryDocumentSnapshot.getLong("drive_shot").floatValue();

                                }

                                if (shortShot == 0)
                                    shortShot = 1;
                                if (mediumShot == 0)
                                    mediumShot = 1;
                                if (longShot == 0)
                                    longShot = 1;
                                if (runningShot == 0)
                                    runningShot = 1;
                                if (driveShot == 0)
                                    driveShot = 1;


                                float totShort = shortScore/shortShot*100;
                                float totMedium = mediumScore/mediumShot*100;
                                float totLong = longScore/longShot*100;
                                float totRunning = runningScore/runningShot*100;
                                float totdrive = driveScore/driveShot*100;

                                ArrayList<RadarEntry> entries1 = new ArrayList<>();

                                entries1.add(new RadarEntry(totShort));
                                entries1.add(new RadarEntry(totMedium));
                                entries1.add(new RadarEntry(totLong));
                                entries1.add(new RadarEntry(totRunning));
                                entries1.add(new RadarEntry(totdrive));

                                RadarDataSet set1 = new RadarDataSet(entries1, monthName+" "+year);
                                set1.setColor(getResources().getColor(R.color.colorPrimary));
                                set1.setFillColor(getResources().getColor(R.color.colorPrimary));
                                set1.setDrawFilled(true);
                                set1.setFillAlpha(180);
                                set1.setLineWidth(2f);
                                set1.setDrawHighlightCircleEnabled(true);
                                set1.setDrawHighlightIndicators(false);

                                ArrayList<IRadarDataSet> sets = new ArrayList<>();
                                sets.add(radarChart.getData().getDataSetByIndex(0));
                                sets.add(set1);
                                radarChart.clear();
                                radarChart.setData(null);
                                radarChart.notifyDataSetChanged();
                                radarChart.invalidate();

                                RadarData data = new RadarData(sets);
                                data.setValueTextSize(8f);
                                data.setDrawValues(false);
                                data.setValueTextColor(Color.WHITE);

                                YAxis yAxis = radarChart.getYAxis();
                                yAxis.setLabelCount(5, false);
                                yAxis.setTextSize(9f);
                                yAxis.setAxisMinimum(0);
                                yAxis.setAxisMaximum(90);
                                yAxis.setDrawLabels(false);

//                    radarProgress.setVisibility(View.GONE);

                                radarChart.setData(data);
                                radarChart.invalidate();
                                radarChart.animateXY(1400, 1400, Easing.EaseInOutQuad);

                            }else {
                                if (radarChart.getData().getDataSetCount() == 2){
                                    ArrayList<IRadarDataSet> sets = new ArrayList<>();
                                    sets.add( radarChart.getData().getDataSetByIndex(0));


                                    radarChart.clear();
//                                radarChart.setData(null);
//                                radarChart.notifyDataSetChanged();
                                    radarChart.invalidate();

                                    RadarData data = new RadarData(sets);
                                    data.setValueTextSize(8f);
                                    data.setDrawValues(false);
                                    data.setValueTextColor(Color.WHITE);


                                    YAxis yAxis = radarChart.getYAxis();
                                    yAxis.setLabelCount(5, false);
                                    yAxis.setTextSize(9f);
                                    yAxis.setAxisMinimum(0);
                                    yAxis.setAxisMaximum(90);
                                    yAxis.setDrawLabels(false);

                                    radarChart.setData(data);
                                    radarChart.invalidate();
                                    radarChart.animateXY(1400, 1400, Easing.EaseInOutQuad);
                                }
                                Toast.makeText(getActivity(), "No data available for selected month", Toast.LENGTH_SHORT).show();
                            }



                        }


//                        Toast.makeText(getActivity(), "No data available for selected month", Toast.LENGTH_SHORT).show();

                    }
                });

    }

    private void setDataBasedMonth(String day, String month, final String year, final String monthName) {

        Query query = FirebaseFirestore.getInstance().collection("users").document(user.getUid()).collection("training_data");

        query.whereEqualTo("month", month).whereEqualTo("year",year).orderBy("day").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            float shortScore = 0, shortShot = 0;
                            float mediumScore = 0, mediumShot = 0;
                            float longScore = 0, longShot = 0;
                            float runningScore = 0, runningShot = 0;
                            float driveScore = 0, driveShot = 0;

                            for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()){

                                shortScore = shortScore + queryDocumentSnapshot.getLong("short_score").floatValue();
                                shortShot = shortShot + queryDocumentSnapshot.getLong("short_shot").floatValue();

                                mediumScore = mediumScore + queryDocumentSnapshot.getLong("medium_score").floatValue();
                                mediumShot = mediumShot + queryDocumentSnapshot.getLong("medium_shot").floatValue();

                                longScore = longScore + queryDocumentSnapshot.getLong("long_score").floatValue();
                                longShot = longShot + queryDocumentSnapshot.getLong("long_shot").floatValue();

                                runningScore = runningScore + queryDocumentSnapshot.getLong("running_score").floatValue();
                                runningShot = runningShot + queryDocumentSnapshot.getLong("running_shot").floatValue();

                                driveScore = driveScore + queryDocumentSnapshot.getLong("drive_score").floatValue();
                                driveShot = driveShot + queryDocumentSnapshot.getLong("drive_shot").floatValue();

                                System.out.println(driveScore+" "+driveShot+" "+runningScore+" "+runningShot+" "+longScore+" "+longShot+" "+mediumScore+" "+
                                        mediumShot+" "+shortScore+" "+shortShot);

                            }

                            if (shortShot == 0)
                                shortShot = 1;
                            if (mediumShot == 0)
                                mediumShot = 1;
                            if (longShot == 0)
                                longShot = 1;
                            if (runningShot == 0)
                                runningShot = 1;
                            if (driveShot == 0)
                                driveShot = 1;


                            float totShort = shortScore/shortShot*100;
                            float totMedium = mediumScore/mediumShot*100;
                            float totLong = longScore/longShot*100;
                            float totRunning = runningScore/runningShot*100;
                            float totdrive = driveScore/driveShot*100;

                            ArrayList<RadarEntry> entries1 = new ArrayList<>();

                            entries1.add(new RadarEntry(totShort));
                            entries1.add(new RadarEntry(totMedium));
                            entries1.add(new RadarEntry(totLong));
                            entries1.add(new RadarEntry(totRunning));
                            entries1.add(new RadarEntry(totdrive));

                            RadarDataSet set1 = new RadarDataSet(entries1, monthName+" "+year);
                            set1.setColor(getResources().getColor(R.color.orange_accent));
                            set1.setFillColor(getResources().getColor(R.color.orange_accent));
                            set1.setDrawFilled(true);
                            set1.setFillAlpha(180);
                            set1.setLineWidth(2f);
                            set1.setDrawHighlightCircleEnabled(true);
                            set1.setDrawHighlightIndicators(false);

                            ArrayList<IRadarDataSet> sets = new ArrayList<>();
                            sets.add(set1);
                            radarChart.clear();
                            radarChart.setData(null);
                            radarChart.notifyDataSetChanged();
                            radarChart.invalidate();

                            RadarData data = new RadarData(sets);
                            data.setValueTextSize(8f);
                            data.setDrawValues(false);
                            data.setValueTextColor(Color.WHITE);

                            YAxis yAxis = radarChart.getYAxis();
                            yAxis.setLabelCount(5, false);
                            yAxis.setTextSize(9f);
                            yAxis.setAxisMinimum(0);
                            yAxis.setAxisMaximum(90);
                            yAxis.setDrawLabels(false);

//                    radarProgress.setVisibility(View.GONE);

                            radarChart.setData(data);
                            radarChart.invalidate();
                            radarChart.animateXY(1400, 1400, Easing.EaseInOutQuad);

                        }
                        else {
                            radarChart.clear();
                            radarChart.invalidate();
                        }

//                        Toast.makeText(getActivity(), "No data available for selected month", Toast.LENGTH_SHORT).show();

                    }
                });

    }

    private void setDataBasedDate(String passDay, String s, int year, String selectedDate) {

        float shortScore, shortShot;
        float mediumScore, mediumShot;
        float longScore, longShot;
        float runningScore, runningShot;
        float driveScore, driveShot;


        ////////////////////////////////////
        ////////////////////////////////////
        ////////////////////////////////////
        ////////////////////////////////////

        firebaseFirestore.collection("users").document(user.getUid()).collection("training_data")
                .document(passDay+s+year).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()){

                            float shortScore = Float.valueOf(documentSnapshot.getLong("short_score")),
                                    shortShot = Float.valueOf(documentSnapshot.getLong("short_shot"));
                            float mediumScore = Float.valueOf(documentSnapshot.getLong("medium_score")),
                                    mediumShot = Float.valueOf(documentSnapshot.getLong("medium_shot"));
                            float longScore = Float.valueOf(documentSnapshot.getLong("long_score")),
                                    longShot = Float.valueOf(documentSnapshot.getLong("long_shot"));
                            float runningScore = Float.valueOf(documentSnapshot.getLong("running_score")),
                                    runningShot = Float.valueOf(documentSnapshot.getLong("running_shot"));
                            float driveScore = Float.valueOf(documentSnapshot.getLong("drive_score")),
                                    driveShot = Float.valueOf(documentSnapshot.getLong("drive_shot"));

                            if (shortShot == 0)
                                shortShot = 1;
                            if (mediumShot == 0)
                                mediumShot = 1;
                            if (longShot == 0)
                                longShot = 1;
                            if (runningShot == 0)
                                runningShot = 1;
                            if (driveShot == 0)
                                driveShot = 1;


                            float totalShort = (shortScore / shortShot) * 100;
                            float totalMedium = (mediumScore / mediumShot) * 100;
                            float totalLong = (longScore / longShot) * 100;
                            float totalRunning = (runningScore / runningShot) * 100;
                            float totalDrive = (driveScore / driveShot) * 100;

                            System.out.println(shortScore + " " + shortShot + " " + mediumScore + " " + mediumShot);
                            System.out.println(totalShort + " " + totalMedium + " " + totalLong + " " + totalRunning + " " + totalDrive);

                            ArrayList<RadarEntry> entries1 = new ArrayList<>();

//                            String legend = "";
                            String date = documentSnapshot.getString("day")+" "+documentSnapshot.get("month")+" "+documentSnapshot.getString("year");

//                            if (date.equals(day+" "+month+" "+this.year)){
//                                legend = "Today";
//                            } else {
//                                legend = date;
//                            }

                            // NOTE: The order of the entries when being added to the entries array determines their position around the center of
                            // the chart.
                            entries1.add(new RadarEntry(totalShort));
                            entries1.add(new RadarEntry(totalMedium));
                            entries1.add(new RadarEntry(totalLong));
                            entries1.add(new RadarEntry(totalRunning));
                            entries1.add(new RadarEntry(totalDrive));

                            RadarDataSet set1 = new RadarDataSet(entries1, date+"   ");
                            set1.setColor(getResources().getColor(R.color.colorAccent));
                            set1.setFillColor(getResources().getColor(R.color.colorAccent));
                            set1.setDrawFilled(true);
                            set1.setFillAlpha(180);
                            set1.setLineWidth(2f);
                            set1.setDrawHighlightCircleEnabled(true);
                            set1.setDrawHighlightIndicators(false);

                            ArrayList<IRadarDataSet> sets = new ArrayList<>();

                            sets.add( radarChart.getData().getDataSetByIndex(0));
                            sets.add(set1);
                            radarChart.clear();
                            radarChart.setData(null);
                            radarChart.notifyDataSetChanged();
                            radarChart.invalidate();

                            RadarData data = new RadarData(sets);
                            data.setValueTextSize(8f);
                            data.setDrawValues(false);
                            data.setValueTextColor(Color.WHITE);

                            YAxis yAxis = radarChart.getYAxis();
                            yAxis.setLabelCount(5, false);
                            yAxis.setTextSize(9f);
                            yAxis.setAxisMinimum(0);
                            yAxis.setAxisMaximum(90);
                            yAxis.setDrawLabels(false);

//                    radarProgress.setVisibility(View.GONE);

                            radarChart.setData(data);
                            radarChart.invalidate();
                            radarChart.animateXY(1400, 1400, Easing.EaseInOutQuad);

                        }
                        else {
                            if (radarChart.getData().getDataSetCount() == 2){
                                ArrayList<IRadarDataSet> sets = new ArrayList<>();
                                sets.add( radarChart.getData().getDataSetByIndex(0));

                                radarChart.clear();
                                radarChart.setData(null);
                                radarChart.notifyDataSetChanged();
                                radarChart.invalidate();

                                RadarData data = new RadarData(sets);
                                data.setValueTextSize(8f);
                                data.setDrawValues(false);
                                data.setValueTextColor(Color.WHITE);

                                YAxis yAxis = radarChart.getYAxis();
                                yAxis.setLabelCount(5, false);
                                yAxis.setTextSize(9f);
                                yAxis.setAxisMinimum(0);
                                yAxis.setAxisMaximum(90);
                                yAxis.setDrawLabels(false);

                                radarChart.setData(data);
                                radarChart.invalidate();
                                radarChart.animateXY(1400, 1400, Easing.EaseInOutQuad);
                            }
                            Toast.makeText(getActivity(), "No data available for selected date", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

//        Query query = firebaseFirestore.collection("Users").document(getArguments().getString("id")).collection("training_data")
//                .whereEqualTo("month", month).orderBy("day", Query.Direction.DESCENDING).limit(1);
//
//        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                if (task.isSuccessful()) {
//
//                    for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()) {
//
//
//
//
//                    }
//
//                } else {
//                    radarChart.setData(null);
////                    radarProgress.setVisibility(View.GONE);
//                    radarChart.invalidate();
//
//                }
//
//            }
//        });


    }


    private void generateRadarBasedDate(String dayOfMonth, String s, int year, String legend1) {

        System.out.println(dayOfMonth+" "+s+" "+year);
        radarChart.getDescription().setEnabled(false);

        radarChart.setWebLineWidth(1f);
        radarChart.setWebColor(getResources().getColor(R.color.light_dark_purple));
        radarChart.setWebLineWidthInner(1f);
        radarChart.setWebColorInner(getResources().getColor(R.color.light_dark_purple));
        radarChart.setWebAlpha(100);

        setData(dayOfMonth, s, year, legend1);

        radarChart.animateXY(1400, 1400, Easing.EaseInOutQuad);

        XAxis xAxis = radarChart.getXAxis();
        xAxis.setTextSize(12);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new ValueFormatter() {

            private final String[] mActivities = new String[]{"short", "Medium", "Long", "Running", "Drive"};

            @Override
            public String getFormattedValue(float value) {
                return mActivities[(int) value % mActivities.length];
            }
        });
        xAxis.setTextColor(getResources().getColor(R.color.light_dark_purple));

        YAxis yAxis = radarChart.getYAxis();
        yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0);
        yAxis.setAxisMaximum(90);
        yAxis.setDrawLabels(false);

        Legend l = radarChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
//        l.setXEntrySpace(5f);
//        l.setYEntrySpace(5f);
        l.setTextColor(getResources().getColor(R.color.light_dark_purple));
        l.setTextSize(12);
    }

    private void setData(final String dayOfMonth, final String s, final int year, final String legend1) {

//        radarProgress.setVisibility(View.VISIBLE);

        float shortScore, shortShot;
        float mediumScore, mediumShot;
        float longScore, longShot;
        float runningScore, runningShot;
        float driveScore, driveShot;

        final DocumentReference documentReference = firebaseFirestore.document("users/" + user.getUid() + "/training_data/" + dayOfMonth + s + year);

        ////////////////////////////////////
        ////////////////////////////////////
        ////////////////////////////////////
        ////////////////////////////////////

        Query query = firebaseFirestore.collection("users").document(user.getUid()).collection("training_data")
                .orderBy("year", Query.Direction.DESCENDING)
                .orderBy("month_no", Query.Direction.DESCENDING).orderBy("day", Query.Direction.DESCENDING).limit(1);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()) {


                        float shortScore = Float.valueOf(queryDocumentSnapshot.getLong("short_score")),
                                shortShot = Float.valueOf(queryDocumentSnapshot.getLong("short_shot"));
                        float mediumScore = Float.valueOf(queryDocumentSnapshot.getLong("medium_score")),
                                mediumShot = Float.valueOf(queryDocumentSnapshot.getLong("medium_shot"));
                        float longScore = Float.valueOf(queryDocumentSnapshot.getLong("long_score")),
                                longShot = Float.valueOf(queryDocumentSnapshot.getLong("long_shot"));
                        float runningScore = Float.valueOf(queryDocumentSnapshot.getLong("running_score")),
                                runningShot = Float.valueOf(queryDocumentSnapshot.getLong("running_shot"));
                        float driveScore = Float.valueOf(queryDocumentSnapshot.getLong("drive_score")),
                                driveShot = Float.valueOf(queryDocumentSnapshot.getLong("drive_shot"));

                        if (shortShot == 0)
                            shortShot = 1;
                        if (mediumShot == 0)
                            mediumShot = 1;
                        if (longShot == 0)
                            longShot = 1;
                        if (runningShot == 0)
                            runningShot = 1;
                        if (driveShot == 0)
                            driveShot = 1;


                        float totalShort = (shortScore / shortShot) * 100;
                        float totalMedium = (mediumScore / mediumShot) * 100;
                        float totalLong = (longScore / longShot) * 100;
                        float totalRunning = (runningScore / runningShot) * 100;
                        float totalDrive = (driveScore / driveShot) * 100;

                        System.out.println(shortScore + " " + shortShot + " " + mediumScore + " " + mediumShot);
                        System.out.println(totalShort + " " + totalMedium + " " + totalLong + " " + totalRunning + " " + totalDrive);

                        ArrayList<RadarEntry> entries1 = new ArrayList<>();

                        String legend = "";
                        String date = queryDocumentSnapshot.getString("day")+" "+queryDocumentSnapshot.get("month")+" "+queryDocumentSnapshot.getString("year");

                        if (date.equals(day+" "+month+" "+year)){
                            radarDateButtonText.setText("Today");
                            legend = "Today";
                        } else {
                            radarDateButtonText.setText(date.substring(0,6));
                            legend = date;
                        }

                        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
                        // the chart.
                        entries1.add(new RadarEntry(totalShort));
                        entries1.add(new RadarEntry(totalMedium));
                        entries1.add(new RadarEntry(totalLong));
                        entries1.add(new RadarEntry(totalRunning));
                        entries1.add(new RadarEntry(totalDrive));

                        RadarDataSet set1 = new RadarDataSet(entries1, legend);
                        set1.setColor(getResources().getColor(R.color.orange_accent));
                        set1.setFillColor(getResources().getColor(R.color.orange_accent));
                        set1.setDrawFilled(true);
                        set1.setFillAlpha(180);
                        set1.setLineWidth(2f);
                        set1.setDrawHighlightCircleEnabled(true);
                        set1.setDrawHighlightIndicators(false);

                        ArrayList<IRadarDataSet> sets = new ArrayList<>();
                        sets.add(set1);

                        RadarData data = new RadarData(sets);
                        data.setValueTextSize(8f);
                        data.setDrawValues(false);
                        data.setValueTextColor(Color.WHITE);

//                    radarProgress.setVisibility(View.GONE);

                        radarChart.setData(data);
                        radarChart.invalidate();

                    }

                } else {
                    radarChart.setData(null);
//                    radarProgress.setVisibility(View.GONE);
                    radarChart.invalidate();

                }

            }
        });

    }


    private void generateDailyBarChart(String mMonth, String mYear) {

        barChart.setNoDataTextColor(getResources().getColor(R.color.grayish));
        barChart.getDescription().setEnabled(false);

        barChart.setMaxVisibleValueCount(5);
        barChart.setPinchZoom(false);
        barChart.setScaleEnabled(false);

        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);
        barChart.setNoDataTextColor(getResources().getColor(R.color.grayish));

        ValueFormatter dailyValueFormatter = new DailyValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(dailyValueFormatter);
        xAxis.setLabelCount(12);
        xAxis.setTextColor(getResources().getColor(R.color.dark_purple));


        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setLabelCount(5, true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMaximum(100f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setTextColor(getResources().getColor(R.color.dark_purple));

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, true);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setTextColor(getResources().getColor(R.color.grayish));

        barChart.getAxisLeft().setDrawGridLines(false);

        // add a nice and smooth animation


        barChart.getLegend().setEnabled(false);
        barChart.setData(null);
        barLoading.setVisibility(View.VISIBLE);
        setDataBarChartDaily(mMonth, mYear);
        barChart.animateY(1500);
    }

    private void setDataBarChartDaily(final String mMonth, String mYear) {

        Query query = firebaseFirestore.collection("users").document(user.getUid()).collection("training_data")
                .whereEqualTo("month", mMonth).whereEqualTo("year", mYear).orderBy("day", Query.Direction.ASCENDING);


        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    ArrayList<BarEntry> barEntriesDaily = new ArrayList<>();
                    int entry = 0;

                    for (int i = 0; i < 33; i++) {

                        if (mMonth.equals("Apr") || mMonth.equals("Jun") || mMonth.equals("Sep") ||
                                mMonth.equals("Nov")) {
                            barEntriesDaily.add(new BarEntry(i, 0));
                        } else if (mMonth.equals("Feb")) {
                            if (i < 30)
                                barEntriesDaily.add(new BarEntry(i, 0));
                        } else
                            barEntriesDaily.add(new BarEntry(i, 0));
                    }

                    for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()) {

                        float percentage = (Float.valueOf(queryDocumentSnapshot.getLong("score")) / Float.valueOf(queryDocumentSnapshot.getLong("shot"))) * 100;
                        System.out.println("day : " + queryDocumentSnapshot.getString("day") + " percentage : " + percentage
                                + queryDocumentSnapshot.getLong("score") + queryDocumentSnapshot.getLong("shot"));
                        barEntriesDaily.add(new BarEntry(Float.valueOf(queryDocumentSnapshot.getString("day")), percentage));

                    }
                    BarDataSet barDataSetDaily = new BarDataSet(barEntriesDaily, "Daily accuracy (%)");

                    List<GradientColor> gradientColors = new ArrayList<>();
                    gradientColors.add(new GradientColor(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorPrimaryDark)));
                    barDataSetDaily.setColor(Color.rgb(108,137,195));

                    BarData barDataDaily = new BarData(barDataSetDaily);
                    barChart.setData(barDataDaily);
                    barChart.setFitBars(false);
                    barLoading.setVisibility(View.GONE);
                    barChart.invalidate();
                    barChart.animateY(1500);
                } else{
                    barLoading.setVisibility(View.GONE);
                    barChart.setNoDataTextColor(getResources().getColor(R.color.orange_accent));
                    barChart.invalidate();
                }

            }
        });
    }


    private void generateWeeklyBarChart(String mMonth, String mYear) {

        barChart.setNoDataTextColor(getResources().getColor(R.color.grayish));
        barChart.getDescription().setEnabled(false);

        barChart.setMaxVisibleValueCount(5);
        barChart.setPinchZoom(false);
        barChart.setScaleEnabled(false);

        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);

        ValueFormatter weeklyFormat = new WeeklyValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(5);
        xAxis.setValueFormatter(weeklyFormat);
        xAxis.setTextColor(getResources().getColor(R.color.dark_purple));


        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setLabelCount(5, true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setTextColor(getResources().getColor(R.color.dark_purple));

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, true);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setTextColor(getResources().getColor(R.color.grayish));

        barChart.getAxisLeft().setDrawGridLines(false);

        // add a nice and smooth animation


        barChart.getLegend().setEnabled(false);
        barChart.setData(null);
        barLoading.setVisibility(View.VISIBLE);
        setDataBarChartWeekly(mMonth, mYear);
        barChart.animateY(1500);
    }

    private void setDataBarChartWeekly(String mMonth, String mYear) {

        Query query = firebaseFirestore.collection("users").document(user.getUid()).collection("training_data_accuracy_weekly")
                .whereEqualTo("month", mMonth).whereEqualTo("year", mYear).orderBy("week");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    ArrayList<BarEntry> barEntriesWeekly = new ArrayList<>();

                    barEntriesWeekly.add(new BarEntry(Float.valueOf(1), 0));
                    barEntriesWeekly.add(new BarEntry(Float.valueOf(2), 0));
                    barEntriesWeekly.add(new BarEntry(Float.valueOf(3), 0));
                    barEntriesWeekly.add(new BarEntry(Float.valueOf(4), 0));
                    barEntriesWeekly.add(new BarEntry(Float.valueOf(5), 0));

                    for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()) {
                        String week = queryDocumentSnapshot.getString("week").substring(4);

                        float shot = 1;
                        if (queryDocumentSnapshot.getLong("shot") != 0) {
                            shot = queryDocumentSnapshot.getLong("shot");
                        }

                        float percentage = Float.valueOf(queryDocumentSnapshot.getLong("score")) / shot * 100;

                        barEntriesWeekly.add(new BarEntry(Float.valueOf(week), percentage));
                    }
                    BarDataSet barDataSetWeekly = new BarDataSet(barEntriesWeekly, "Accuracy (%)");

                    List<GradientColor> gradientColors = new ArrayList<>();
                    gradientColors.add(new GradientColor(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorPrimaryDark)));
                    barDataSetWeekly.setColor(Color.rgb(108,137,195));

                    BarData barDataWeekly = new BarData(barDataSetWeekly);
                    barChart.setData(barDataWeekly);
                    barChart.setFitBars(false);
                    barLoading.setVisibility(View.GONE);
                    barChart.invalidate();
                    barChart.animateY(1500);

                } else{
                    barLoading.setVisibility(View.GONE);
                    barChart.setNoDataTextColor(getResources().getColor(R.color.orange_accent));
                    barChart.invalidate();
                }


            }
        });
    }


    private void generateMonthlyBarChart(String mYear) {

        barChart.setNoDataTextColor(getResources().getColor(R.color.grayish));
        barChart.getDescription().setEnabled(false);

        barChart.setMaxVisibleValueCount(5);
        barChart.setPinchZoom(false);
        barChart.setScaleEnabled(false);

        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);

        ValueFormatter monthlyFormat = new MonthlyValueFormatter(barChart);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(12);
        xAxis.setValueFormatter(monthlyFormat);
        xAxis.setTextColor(getResources().getColor(R.color.dark_purple));


        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setLabelCount(5, true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setTextColor(getResources().getColor(R.color.dark_purple));

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, true);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setTextColor(getResources().getColor(R.color.grayish));

        barChart.getAxisLeft().setDrawGridLines(false);

        // add a nice and smooth animation


        barChart.getLegend().setEnabled(false);

        barChart.setData(null);
        barLoading.setVisibility(View.VISIBLE);
        setDataBarChartMonthly(mYear);

        barChart.animateY(1500);
    }

    private void setDataBarChartMonthly(String mYear) {

        Query query = firebaseFirestore.collection("users").document(user.getUid()).collection("training_data_accuracy_monthly")
                .whereEqualTo("year", mYear).orderBy("no");

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    ArrayList<BarEntry> barEntriesMonthly = new ArrayList<>();
                    barEntriesMonthly.add(new BarEntry(1, 0));
                    barEntriesMonthly.add(new BarEntry(2, 0));
                    barEntriesMonthly.add(new BarEntry(3, 0));
                    barEntriesMonthly.add(new BarEntry(4, 0));
                    barEntriesMonthly.add(new BarEntry(5, 0));
                    barEntriesMonthly.add(new BarEntry(6, 0));
                    barEntriesMonthly.add(new BarEntry(7, 0));
                    barEntriesMonthly.add(new BarEntry(8, 0));
                    barEntriesMonthly.add(new BarEntry(9, 0));
                    barEntriesMonthly.add(new BarEntry(10, 0));
                    barEntriesMonthly.add(new BarEntry(11, 0));
                    barEntriesMonthly.add(new BarEntry(12, 0));

                    for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()) {

                        float index = queryDocumentSnapshot.getLong("no");
                        float shot = 1;
                        if (queryDocumentSnapshot.getLong("shot") != 0) {
                            shot = queryDocumentSnapshot.getLong("shot");
                        }

                        float percentage = Float.valueOf(queryDocumentSnapshot.getLong("score")) / shot * 100;
                        barEntriesMonthly.add(new BarEntry(index, percentage));

                    }
                    BarDataSet barDataSetMonthly = new BarDataSet(barEntriesMonthly, "Accuracy (%)");

                    List<GradientColor> gradientColors = new ArrayList<>();
                    gradientColors.add(new GradientColor(getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorPrimaryDark)));
                    barDataSetMonthly.setColor(Color.rgb(108,137,195));

                    BarData barDataMonthly = new BarData(barDataSetMonthly);
                    barChart.setData(barDataMonthly);
                    barChart.setFitBars(false);
                    barLoading.setVisibility(View.GONE);
                    barChart.invalidate();
                    barChart.animateY(1500);

                } else{
                    barLoading.setVisibility(View.GONE);
                    barChart.setNoDataTextColor(getResources().getColor(R.color.orange_accent));
                    barChart.invalidate();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.
                checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        // This verification should be done during onStart() because the system calls
        // this method when the user returns to the activity, which ensures the desired
        // location provider is enabled each time the activity resumes from the stopped state.
        LocationManager locationManager =
                (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!gpsEnabled) {
            // Build an alert dialog here that requests that the user enable
            // the location services, then when the user clicks the "OK" button,
            enableLocationSettings();
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, listener);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }

    private void enableLocationSettings() {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(settingsIntent);
    }


    private final LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            LatLng zeroZero = new LatLng(0.0, 0.0);

            if (location.equals(zeroZero) || location == null) {
                if (ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
                Location locationNetwork = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

                System.out.println(location.getLatitude()+" ini "+location.getLongitude()+" "+dbLatitude+dbLongitude+dbRadius);
                oldLatitude = latitude;
                oldLongitude = longitude;

                latitude = locationNetwork.getLatitude();
                longitude = locationNetwork.getLongitude();

                Location oldLocation = new Location("oldLocation");

                oldLocation.setLatitude(oldLatitude);
                oldLocation.setLongitude(oldLongitude);

                Location dbLocation = new Location("dbLocation");
                dbLocation.setLatitude(dbLatitude);
                dbLocation.setLongitude(dbLongitude);

                if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //entered
//                Toast.makeText(getContext(),"Entered Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Entered Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //inside
//                Toast.makeText(getContext(),"Inside Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Inside Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) > dbRadius){    //exited
//                Toast.makeText(getContext(),"Exited Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Exited Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) > dbRadius){   //outside
//                Toast.makeText(getContext(),"Outside Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Outside Training Location");
                }


                Toast.makeText(getActivity(), "Location not found", Toast.LENGTH_SHORT).show();
            }
            else {

                System.out.println(location.getLatitude()+" ini "+location.getLongitude()+" "+dbLatitude+dbLongitude+dbRadius);
                oldLatitude = latitude;
                oldLongitude = longitude;

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                Location oldLocation = new Location("oldLocation");

                oldLocation.setLatitude(oldLatitude);
                oldLocation.setLongitude(oldLongitude);

                Location dbLocation = new Location("dbLocation");
                dbLocation.setLatitude(dbLatitude);
                dbLocation.setLongitude(dbLongitude);

                if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //entered
//                Toast.makeText(getActivity(),"Entered Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Entered Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //inside
//                Toast.makeText(getActivity(),"Inside Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Inside Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) > dbRadius){    //exited
//                Toast.makeText(getActivity(),"Exited Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Exited Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) > dbRadius){   //outside
//                Toast.makeText(getActivity(),"Outside Geofence",Toast.LENGTH_SHORT).show();
//                    geofenceText.setText("Outside Training Location");
                }

            }

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private boolean insideGeofence() {
        Location currentLocation = new Location("currentLocation");
        currentLocation.setLatitude(latitude);
        currentLocation.setLongitude(longitude);

        Location dbLocation = new Location("dbLocation");
        dbLocation.setLatitude(dbLatitude);
        dbLocation.setLongitude(dbLongitude);

        System.out.println(currentLocation.distanceTo(dbLocation)+" "+dbRadius);
        double myDistance = currentLocation.distanceTo(dbLocation);

        if (myDistance <= dbRadius) {
            return true;
        } else return false;
    }

}
