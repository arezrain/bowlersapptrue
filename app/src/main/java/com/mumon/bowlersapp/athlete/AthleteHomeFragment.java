package com.mumon.bowlersapp.athlete;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.geofence.Constants;
import com.mumon.bowlersapp.geofence.GeofenceBroadcastReciever;
import com.mumon.bowlersapp.geofence.GeofenceErrorMessages;
import com.mumon.bowlersapp.geofence.GeofenceSetting;
import com.mumon.bowlersapp.model.Users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AthleteHomeFragment extends Fragment {

    private CardView trainingData, teamList, trainingNote, trainingLocation;
    public NavController navController = null;
    private RelativeLayout activityBar;
    private Window window;
    Toolbar toolbar;
    private Button logout;
    private TextView geofenceText, name, email;
    Users users;

    private double latitude;
    private double longitude;
    double tempLatitude;
    double tempLongitude;
    private double oldLatitude;
    private double oldLongitude;

    private double dbLatitude;
    private double dbLongitude;
    private float dbRadius;
    private LocationManager locationManager;
    private LocationProvider locationProvider;
    Location currentLocation;
    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.athlete_home, container, false);
        toolbar = getActivity().findViewById(R.id.toolbar);
        window = getActivity().getWindow();
        toolbar.setBackgroundColor(Color.argb(0,57,124,192));
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryAthleteDark));

        trainingData = view.findViewById(R.id.athlete_home_view_data_button);
        teamList = view.findViewById(R.id.athlete_home_team_list_button);
        trainingNote = view.findViewById(R.id.athlete_home_training_note_button);
        trainingLocation = view.findViewById(R.id.athlete_home_location_button);
        geofenceText = view.findViewById(R.id.geofence_text);
        name = view.findViewById(R.id.athlete_home_name);
        email = view.findViewById(R.id.athlete_home_email);



        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.
                checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setCostAllowed(false);

        String providerName = locationManager.getBestProvider(criteria, true);


        fetchUserData();

        trainingData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
//                Bundle bundle = new Bundle();
//
//                String check = users.getName();
//                if (check != null){
                    Navigation.findNavController(view).navigate(R.id.action_athlete_home_fragment_to_athleteViewTrainingDataFragment);
//                }
            }
        });

        teamList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Navigation.findNavController(view).navigate(R.id.action_athlete_home_fragment_to_athleteTeamListFragment);
            }
        });

        trainingNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_athlete_home_fragment_to_athleteTrainingNote);
            }
        });

        trainingLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data", 0);
                firebaseDatabase.getReference("GeofenceSetting/"+sharedPreferences.getString("team_key",""))
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                GeofenceSetting geofenceSetting = dataSnapshot.getValue(GeofenceSetting.class);
                                double dbLat = geofenceSetting.getLatitude();
                                double dbLon = geofenceSetting.getLongitude();

                                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                        Uri.parse("http://maps.google.com/maps?q=loc:" + dbLat + "," + dbLon + "(Training Location)"));
                                startActivity(intent);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }
        });

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data", 0);
        firebaseDatabase.getReference("GeofenceSetting/" + sharedPreferences.getString("team_key", "")).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            GeofenceSetting geofenceSetting = dataSnapshot.getValue(GeofenceSetting.class);
                            dbLatitude = geofenceSetting.getLatitude();
                            dbLongitude = geofenceSetting.getLongitude();
                            dbRadius = geofenceSetting.getRadius();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }
        );

        return view;
    }

    private boolean insideGeofence() {
        Location currentLocation = new Location("currentLocation");
        currentLocation.setLatitude(latitude);
        currentLocation.setLongitude(longitude);

        Location dbLocation = new Location("dbLocation");
        dbLocation.setLatitude(dbLatitude);
        dbLocation.setLongitude(dbLongitude);

        if (currentLocation.distanceTo(dbLocation) <= dbRadius) {
            return true;
        } else return false;
    }

    @Override
    public void onStart() {
        super.onStart();



        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.
                checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        // This verification should be done during onStart() because the system calls
        // this method when the user returns to the activity, which ensures the desired
        // location provider is enabled each time the activity resumes from the stopped state.
        LocationManager locationManager =
                (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!gpsEnabled) {
            // Build an alert dialog here that requests that the user enable
            // the location services, then when the user clicks the "OK" button,
            enableLocationSettings();
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, listener);
    }

    private void fetchUserData() {
        FirebaseFirestore.getInstance().document("users/"+ FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                users = documentSnapshot.toObject(Users.class);
                name.setText(users.getName());
                email.setText(users.getEmail());
            }
        });

    }

    private void enableLocationSettings() {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(settingsIntent);
    }

    private final LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            LatLng zeroZero = new LatLng(0.0, 0.0);

            if (location.equals(zeroZero) || location == null) {
                if (ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
                Location locationNetwork = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

                System.out.println(location.getLatitude()+" ini "+location.getLongitude()+" "+dbLatitude+dbLongitude+dbRadius);
                oldLatitude = latitude;
                oldLongitude = longitude;

                latitude = locationNetwork.getLatitude();
                longitude = locationNetwork.getLongitude();

                Location oldLocation = new Location("oldLocation");

                oldLocation.setLatitude(oldLatitude);
                oldLocation.setLongitude(oldLongitude);

                Location dbLocation = new Location("dbLocation");
                dbLocation.setLatitude(dbLatitude);
                dbLocation.setLongitude(dbLongitude);

                if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //entered
//                Toast.makeText(getContext(),"Entered Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Entered Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //inside
//                Toast.makeText(getContext(),"Inside Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Inside Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) > dbRadius){    //exited
//                Toast.makeText(getContext(),"Exited Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Exited Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) > dbRadius){   //outside
//                Toast.makeText(getContext(),"Outside Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Outside Training Location");
                }


                Toast.makeText(getActivity(), "Location not found", Toast.LENGTH_SHORT).show();
            }
            else {

                System.out.println(location.getLatitude()+" ini "+location.getLongitude()+" "+dbLatitude+dbLongitude+dbRadius);
                oldLatitude = latitude;
                oldLongitude = longitude;

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                Location oldLocation = new Location("oldLocation");

                oldLocation.setLatitude(oldLatitude);
                oldLocation.setLongitude(oldLongitude);

                Location dbLocation = new Location("dbLocation");
                dbLocation.setLatitude(dbLatitude);
                dbLocation.setLongitude(dbLongitude);

                if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //entered
//                Toast.makeText(getContext(),"Entered Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Entered Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) <= dbRadius){    //inside
//                Toast.makeText(getContext(),"Inside Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Inside Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) <= dbRadius && location.distanceTo(dbLocation) > dbRadius){    //exited
//                Toast.makeText(getContext(),"Exited Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Exited Training Location");
                }
                else if (oldLocation.distanceTo(dbLocation) > dbRadius && location.distanceTo(dbLocation) > dbRadius){   //outside
//                Toast.makeText(getContext(),"Outside Geofence",Toast.LENGTH_SHORT).show();
                    geofenceText.setText("Outside Training Location");
                }

            }

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };


}

