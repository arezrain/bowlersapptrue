package com.mumon.bowlersapp.athlete;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.adapter.AthleteSearchAdapter;
import com.mumon.bowlersapp.model.Users;

public class AthleteRankingFragment extends Fragment {

    private EditText ps1, ps2, ps3, ps4, ps5, ps6, ps7, ps8, ps9, ps10, ps11, ps12, ps13, ps14, ps15, ps16;
    private EditText pt1, pt2, pt3, pt4, pt5, pt6, pt7, pt8, pt9, pt10, pt11, pt12, pt13, pt14, pt15, pt16;

    private EditText os1, os2, os3, os4, os5, os6, os7, os8, os9, os10, os11, os12, os13, os14, os15, os16;
    private EditText ot1, ot2, ot3, ot4, ot5, ot6, ot7, ot8, ot9, ot10, ot11, ot12, ot13, ot14, ot15, ot16;

    private EditText opponentName;
    private NestedScrollView scorecardContainer;

    private Button save, cancel;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference searchRef = db.collection("users");

    private LinearLayout opponentSearch;
    AthleteSearchAdapter mAdapter;

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.athlete_ranking_fragment, container, false);

        setupScorecardEditText(view);

        opponentName = view.findViewById(R.id.athlete_ranking_opponent_name);
        opponentSearch = view.findViewById(R.id.athlete_ranking_search_opponent);
        scorecardContainer = view.findViewById(R.id.scorecard_container);
        opponentSearch.setVisibility(View.GONE);
        scorecardContainer.setVisibility(View.GONE);
        save = view.findViewById(R.id.scorecard_save_button);
        cancel = view.findViewById(R.id.scorecard_cancel_button);

        textChangeListener();

        opponentName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = opponentName.getText().toString().trim();
                if (!check.isEmpty()){
                    opponentSearch.setVisibility(View.VISIBLE);
                    setUpRecyclerView(view,opponentName.getText().toString().trim().toLowerCase());
                    mAdapter.startListening();

                }
                else {
                    opponentSearch.setVisibility(View.GONE);
                }
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("save");
                AlertDialog.Builder save = new AlertDialog.Builder(getActivity());
                save.setMessage("Do you want to save this scorecard?");
                save.setCancelable(true);

                save.setPositiveButton(
                        "Proceed",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                save.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = save.create();
                alert11.show();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("cancel");
                AlertDialog.Builder cancel = new AlertDialog.Builder(getActivity());
                cancel.setMessage("Do you want to discard this scorecard?");
                cancel.setCancelable(true);

                cancel.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().onBackPressed();
                                dialog.cancel();
                            }
                        });

                cancel.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = cancel.create();
                alert11.show();
            }
        });

        return view;
    }

    private void setUpRecyclerView(final View view, String keyword){


        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data",0);
        Query query = searchRef.whereEqualTo("team_key",sharedPreferences.getString("team_key",""))
                .whereEqualTo("role","Athlete").whereArrayContains("keyword", keyword).orderBy("name");

        FirestoreRecyclerOptions<Users> recyclerOptions = new FirestoreRecyclerOptions.Builder<Users>()
                .setQuery(query, Users.class)
                .build();

        mAdapter = new AthleteSearchAdapter(recyclerOptions);

        RecyclerView recyclerView = view.findViewById(R.id.search_recycler);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnItemClickListener(new AthleteSearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {

                final Users users = documentSnapshot.toObject(Users.class);
                String id = documentSnapshot.getId();
                if (id.equals(user.getUid())){
                    Toast.makeText(getActivity(), "Please choose another person", Toast.LENGTH_SHORT).show();
                }
                else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setMessage("You want to choose "+ users.getName()+" as your opponent?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    opponentName.setText(users.getName().toUpperCase());
                                    scorecardContainer.setVisibility(View.VISIBLE);
                                    opponentSearch.setVisibility(View.GONE);
                                    opponentName.clearFocus();
                                    opponentName.setEnabled(false);
                                    mAdapter.stopListening();
                                    dialog.cancel();
                                }
                            });

                    builder1.setNegativeButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    opponentSearch.setVisibility(View.VISIBLE);
                                    scorecardContainer.setVisibility(View.GONE);
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });

    }

    private void textChangeListener() {

        ps1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps1.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        pt1.setText(check);
                        os1.setText("-");
                        ot1.setText("-");
                        ps2.setEnabled(true);
                        os2.setEnabled(true);
                    }
                }
                else {
                    ps2.setEnabled(false);
                    os2.setEnabled(false);
                }
            }
        });

        ps2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps2.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt1.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt1.getText().toString().trim());
                            pt2.setText(String.valueOf(total));
                        }
                        else {
                            pt2.setText(check);
                        }
                        os2.setText("-");
                        ot2.setText(ot1.getText().toString().trim());
                        ps3.setEnabled(true);
                        os3.setEnabled(true);
                    }
                }
                else {
                    ps3.setEnabled(false);
                    os3.setEnabled(false);
                }
            }
        });

        ps3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps3.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt2.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt2.getText().toString().trim());
                            pt3.setText(String.valueOf(total));
                        }
                        else {
                            pt3.setText(check);
                        }
                        os3.setText("-");
                        ot3.setText(ot2.getText().toString().trim());
                        ps4.setEnabled(true);
                        os4.setEnabled(true);
                    }
                }
                else {
                    ps4.setEnabled(false);
                    os4.setEnabled(false);
                }
            }
        });

        ps4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps4.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt3.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt3.getText().toString().trim());
                            pt4.setText(String.valueOf(total));
                        }
                        else {
                            pt4.setText(check);
                        }
                        os4.setText("-");
                        ot4.setText(ot3.getText().toString().trim());
                        ps5.setEnabled(true);
                        os5.setEnabled(true);
                    }
                }
                else {
                    ps5.setEnabled(false);
                    os5.setEnabled(false);
                }
            }
        });

        ps5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps5.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt4.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt4.getText().toString().trim());
                            pt5.setText(String.valueOf(total));
                        }
                        else {
                            pt5.setText(check);
                        }
                        os5.setText("-");
                        ot5.setText(ot4.getText().toString().trim());
                        ps6.setEnabled(true);
                        os6.setEnabled(true);
                    }
                }
                else {
                    ps6.setEnabled(false);
                    os6.setEnabled(false);
                }

            }
        });

        ps6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps6.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt5.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt5.getText().toString().trim());
                            pt6.setText(String.valueOf(total));
                        }
                        else {
                            pt6.setText(check);
                        }
                        os6.setText("-");
                        ot6.setText(ot5.getText().toString().trim());
                        ps7.setEnabled(true);
                        os7.setEnabled(true);
                    }
                }
                else {
                    ps7.setEnabled(false);
                    os7.setEnabled(false);
                }

            }
        });

        ps7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps7.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt6.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt6.getText().toString().trim());
                            pt7.setText(String.valueOf(total));
                        }
                        else {
                            pt7.setText(check);
                        }
                        os7.setText("-");
                        ot7.setText(ot6.getText().toString().trim());
                        ps8.setEnabled(true);
                        os8.setEnabled(true);
                    }
                }
                else {
                    ps8.setEnabled(false);
                    os8.setEnabled(false);
                }

            }
        });

        ps8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps8.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt7.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt7.getText().toString().trim());
                            pt8.setText(String.valueOf(total));
                        }
                        else {
                            pt8.setText(check);
                        }
                        os8.setText("-");
                        ot8.setText(ot7.getText().toString().trim());
                        ps9.setEnabled(true);
                        os9.setEnabled(true);
                    }
                }
                else {
                    ps9.setEnabled(false);
                    os9.setEnabled(false);
                }
            }
        });

        ps9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps9.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt8.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt8.getText().toString().trim());
                            pt9.setText(String.valueOf(total));
                        }
                        else {
                            pt9.setText(check);
                        }
                        os9.setText("-");
                        ot9.setText(ot8.getText().toString().trim());
                        ps10.setEnabled(true);
                        os10.setEnabled(true);
                    }
                }
                else {
                    ps10.setEnabled(false);
                    os10.setEnabled(false);
                }
            }
        });

        ps10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps10.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt9.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt9.getText().toString().trim());
                            pt10.setText(String.valueOf(total));
                        }
                        else {
                            pt10.setText(check);
                        }
                        os10.setText("-");
                        ot10.setText(ot9.getText().toString().trim());
                        ps11.setEnabled(true);
                        os11.setEnabled(true);
                    }
                }
                else {
                    ps11.setEnabled(false);
                    os11.setEnabled(false);
                }
            }
        });

        ps11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps11.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt10.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt10.getText().toString().trim());
                            pt11.setText(String.valueOf(total));
                        }
                        else {
                            pt11.setText(check);
                        }
                        os11.setText("-");
                        ot11.setText(ot10.getText().toString().trim());
                        ps12.setEnabled(true);
                        os12.setEnabled(true);
                    }
                }
                else {
                    ps12.setEnabled(false);
                    os12.setEnabled(false);
                }
            }
        });

        ps12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps12.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt11.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt11.getText().toString().trim());
                            pt12.setText(String.valueOf(total));
                        }
                        else {
                            pt12.setText(check);
                        }
                        os12.setText("-");
                        ot12.setText(ot11.getText().toString().trim());
                        ps13.setEnabled(true);
                        os13.setEnabled(true);
                    }
                }
                else {
                    ps13.setEnabled(false);
                    os13.setEnabled(false);
                }
            }
        });

        ps13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps13.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt12.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt12.getText().toString().trim());
                            pt13.setText(String.valueOf(total));
                        }
                        else {
                            pt13.setText(check);
                        }
                        os13.setText("-");
                        ot13.setText(ot12.getText().toString().trim());
                        ps14.setEnabled(true);
                        os14.setEnabled(true);
                    }
                }
                else {
                    ps14.setEnabled(false);
                    os14.setEnabled(false);
                }
            }
        });

        ps14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps14.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt13.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt13.getText().toString().trim());
                            pt14.setText(String.valueOf(total));
                        }
                        else {
                            pt14.setText(check);
                        }
                        os14.setText("-");
                        ot14.setText(ot13.getText().toString().trim());
                        ps15.setEnabled(true);
                        os15.setEnabled(true);
                    }
                }
                else {
                    ps15.setEnabled(false);
                    os15.setEnabled(false);
                }
            }
        });

        ps15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps15.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt14.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt14.getText().toString().trim());
                            pt15.setText(String.valueOf(total));
                        }
                        else {
                            pt15.setText(check);
                        }
                        os15.setText("-");
                        ot15.setText(ot14.getText().toString().trim());
                        ps16.setEnabled(true);
                        os16.setEnabled(true);
                    }
                }
                else {
                    ps16.setEnabled(false);
                    os15.setEnabled(false);
                }
            }
        });

        ps16.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ps16.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!pt15.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(pt15.getText().toString().trim());
                            pt16.setText(String.valueOf(total));
                        }
                        else {
                            pt16.setText(check);
                        }
                        os16.setText("-");
                        ot16.setText(ot15.getText().toString().trim());
                    }
                }
            }
        });

        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        ////////////////////////////////////////////////

        os1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os1.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        ot1.setText(check);
                        ps1.setText("-");
                        pt1.setText("-");
                        os2.setEnabled(true);
                        ps2.setEnabled(true);
                    }
                }
                else {
                    os2.setEnabled(false);
                    ps2.setEnabled(false);
                }
            }
        });

        os2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os2.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot1.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot1.getText().toString().trim());
                            ot2.setText(String.valueOf(total));
                        }
                        else {
                            ot2.setText(check);
                        }
                        ps2.setText("-");
                        pt2.setText(pt1.getText().toString().trim());
                        ps3.setEnabled(true);
                        os3.setEnabled(true);
                    }
                }
                else {
                    ps3.setEnabled(false);
                    os3.setEnabled(false);
                }
            }
        });

        os3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os3.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot2.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot2.getText().toString().trim());
                            ot3.setText(String.valueOf(total));
                        }
                        else {
                            ot3.setText(check);
                        }
                        ps3.setText("-");
                        pt3.setText(pt2.getText().toString().trim());
                        ps4.setEnabled(true);
                        os4.setEnabled(true);
                    }
                }
                else {
                    ps4.setEnabled(false);
                    os4.setEnabled(false);
                }
            }
        });

        os4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os4.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot3.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot3.getText().toString().trim());
                            ot4.setText(String.valueOf(total));
                        }
                        else {
                            ot4.setText(check);
                        }
                        ps4.setText("-");
                        pt4.setText(pt3.getText().toString().trim());
                        ps5.setEnabled(true);
                        os5.setEnabled(true);
                    }
                }
                else {
                    ps5.setEnabled(false);
                    os5.setEnabled(false);
                }
            }
        });

        os5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os5.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot4.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot4.getText().toString().trim());
                            ot5.setText(String.valueOf(total));
                        }
                        else {
                            ot5.setText(check);
                        }
                        ps5.setText("-");
                        pt5.setText(pt4.getText().toString().trim());
                        ps6.setEnabled(true);
                        os6.setEnabled(true);
                    }
                }
                else {
                    ps6.setEnabled(false);
                    os6.setEnabled(false);
                }
            }
        });

        os6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os6.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot5.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot5.getText().toString().trim());
                            ot6.setText(String.valueOf(total));
                        }
                        else {
                            ot6.setText(check);
                        }
                        ps6.setText("-");
                        pt6.setText(pt5.getText().toString().trim());
                        ps7.setEnabled(true);
                        os7.setEnabled(true);
                    }
                }
                else {
                    ps7.setEnabled(false);
                    os7.setEnabled(false);
                }
            }
        });

        os7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os7.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot6.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot6.getText().toString().trim());
                            ot7.setText(String.valueOf(total));
                        }
                        else {
                            ot7.setText(check);
                        }
                        ps7.setText("-");
                        pt7.setText(pt6.getText().toString().trim());
                        ps8.setEnabled(true);
                        os8.setEnabled(true);
                    }
                }
                else {
                    ps8.setEnabled(false);
                    os8.setEnabled(false);
                }
            }
        });

        os8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os8.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot7.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot7.getText().toString().trim());
                            ot8.setText(String.valueOf(total));
                        }
                        else {
                            ot8.setText(check);
                        }
                        ps8.setText("-");
                        pt8.setText(pt7.getText().toString().trim());
                        ps9.setEnabled(true);
                        os9.setEnabled(true);
                    }
                }
                else {
                    ps9.setEnabled(false);
                    os9.setEnabled(false);
                }
            }
        });

        os9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os9.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot8.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot8.getText().toString().trim());
                            ot9.setText(String.valueOf(total));
                        }
                        else {
                            ot9.setText(check);
                        }
                        ps9.setText("-");
                        pt9.setText(pt8.getText().toString().trim());
                        ps10.setEnabled(true);
                        os10.setEnabled(true);
                    }
                }
                else {
                    ps10.setEnabled(false);
                    os10.setEnabled(false);
                }
            }
        });

        os10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os10.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot9.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot9.getText().toString().trim());
                            ot10.setText(String.valueOf(total));
                        }
                        else {
                            ot10.setText(check);
                        }
                        ps10.setText("-");
                        pt10.setText(pt9.getText().toString().trim());
                        ps11.setEnabled(true);
                        os11.setEnabled(true);
                    }
                }
                else {
                    ps11.setEnabled(false);
                    os11.setEnabled(false);
                }
            }
        });

        os11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os11.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot10.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot10.getText().toString().trim());
                            ot11.setText(String.valueOf(total));
                        }
                        else {
                            ot11.setText(check);
                        }
                        ps11.setText("-");
                        pt11.setText(pt10.getText().toString().trim());
                        ps12.setEnabled(true);
                        os12.setEnabled(true);
                    }
                }
                else {
                    ps12.setEnabled(false);
                    os12.setEnabled(false);
                }
            }
        });

        os12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os12.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot11.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot11.getText().toString().trim());
                            ot12.setText(String.valueOf(total));
                        }
                        else {
                            ot12.setText(check);
                        }
                        ps12.setText("-");
                        pt12.setText(pt11.getText().toString().trim());
                        ps13.setEnabled(true);
                        os13.setEnabled(true);
                    }
                }
                else {
                    ps13.setEnabled(false);
                    os13.setEnabled(false);
                }
            }
        });

        os13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os13.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot12.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot12.getText().toString().trim());
                            ot13.setText(String.valueOf(total));
                        }
                        else {
                            ot13.setText(check);
                        }
                        ps13.setText("-");
                        pt13.setText(pt12.getText().toString().trim());
                        ps14.setEnabled(true);
                        os14.setEnabled(true);
                    }
                }
                else {
                    ps14.setEnabled(false);
                    os14.setEnabled(false);
                }
            }
        });

        os14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os14.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot13.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot13.getText().toString().trim());
                            ot14.setText(String.valueOf(total));
                        }
                        else {
                            ot14.setText(check);
                        }
                        ps14.setText("-");
                        pt14.setText(pt13.getText().toString().trim());
                        ps15.setEnabled(true);
                        os15.setEnabled(true);
                    }
                }
                else {
                    ps15.setEnabled(false);
                    os15.setEnabled(false);
                }
            }
        });

        os15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os15.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot14.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot14.getText().toString().trim());
                            ot15.setText(String.valueOf(total));
                        }
                        else {
                            ot15.setText(check);
                        }
                        ps15.setText("-");
                        pt15.setText(pt14.getText().toString().trim());
                        ps16.setEnabled(true);
                        os16.setEnabled(true);
                    }
                }
                else {
                    ps16.setEnabled(false);
                    os15.setEnabled(false);
                }
            }
        });

        os16.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = os16.getText().toString().trim();
                if (!check.isEmpty()){
                    if (!check.equals("-")){
                        if (!ot15.getText().toString().trim().equals("-")){
                            int total = Integer.valueOf(check)+Integer.valueOf(ot15.getText().toString().trim());
                            ot16.setText(String.valueOf(total));
                        }
                        else {
                            ot16.setText(check);
                        }
                        ps16.setText("-");
                        ps16.setText(pt15.getText().toString().trim());
                    }
                }
            }
        });


        ///////////////////////////////////////////
        ///////////////////////////////////////////
        ///////////////////////////////////////////

        pt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt1.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps2.getText().toString().trim();
                    ps2.setText(set);
                }
            }
        });

        pt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt2.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps3.getText().toString().trim();
                    ps3.setText(set);
                }
            }
        });

        pt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt3.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps4.getText().toString().trim();
                    ps4.setText(set);
                }
            }
        });

        pt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt4.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps5.getText().toString().trim();
                    ps5.setText(set);
                }
            }
        });

        pt5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt5.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps6.getText().toString().trim();
                    ps6.setText(set);
                }

            }
        });

        pt6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt6.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps7.getText().toString().trim();
                    ps7.setText(set);
                }
            }
        });

        pt7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt7.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps8.getText().toString().trim();
                    ps8.setText(set);
                }

            }
        });

        pt8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt8.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps9.getText().toString().trim();
                    ps9.setText(set);
                }
            }
        });

        pt9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt9.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps10.getText().toString().trim();
                    ps10.setText(set);
                }
            }
        });

        pt10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt10.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps11.getText().toString().trim();
                    ps11.setText(set);
                }
            }
        });

        pt11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt11.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps12.getText().toString().trim();
                    ps12.setText(set);
                }
            }
        });

        pt12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt12.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps13.getText().toString().trim();
                    ps13.setText(set);
                }
            }
        });

        pt13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt13.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps14.getText().toString().trim();
                    ps14.setText(set);
                }
            }
        });

        pt14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt14.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps15.getText().toString().trim();
                    ps15.setText(set);
                }
            }
        });

        pt15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = pt15.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = ps16.getText().toString().trim();
                    ps16.setText(set);
                }
            }
        });

        //////////////////////////////////////////
        //////////////////////////////////////////
        //////////////////////////////////////////


        ot1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot1.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os2.getText().toString().trim();
                    os2.setText(set);
                }
            }
        });

        ot2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot2.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os3.getText().toString().trim();
                    os3.setText(set);
                }
            }
        });

        ot3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot3.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os4.getText().toString().trim();
                    os4.setText(set);
                }
            }
        });

        ot4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot4.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os5.getText().toString().trim();
                    os5.setText(set);
                }
            }
        });

        ot5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot5.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os6.getText().toString().trim();
                    os6.setText(set);
                }

            }
        });

        ot6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot6.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os7.getText().toString().trim();
                    os7.setText(set);
                }

            }
        });

        ot7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot7.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os8.getText().toString().trim();
                    os8.setText(set);
                }

            }
        });

        ot8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot8.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os9.getText().toString().trim();
                    os9.setText(set);
                }
            }
        });

        ot9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot9.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os10.getText().toString().trim();
                    os10.setText(set);
                }
            }
        });

        ot10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot10.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os11.getText().toString().trim();
                    os11.setText(set);
                }
            }
        });

        ot11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot11.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os12.getText().toString().trim();
                    os12.setText(set);
                }
            }
        });

        ot12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot12.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os13.getText().toString().trim();
                    os13.setText(set);
                }
            }
        });

        ot13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot13.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os14.getText().toString().trim();
                    os14.setText(set);
                }
            }
        });

        ot14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot14.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os15.getText().toString().trim();
                    os15.setText(set);
                }
            }
        });

        ot15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = ot15.getText().toString().trim();
                if (!check.isEmpty()){
                    String set = os16.getText().toString().trim();
                    os16.setText(set);
                }
            }
        });

    }

    private void setupScorecardEditText(View view) {

        os1 = view.findViewById(R.id.os1);
        os2 = view.findViewById(R.id.os2);
        os3 = view.findViewById(R.id.os3);
        os4 = view.findViewById(R.id.os4);
        os5 = view.findViewById(R.id.os5);
        os6 = view.findViewById(R.id.os6);
        os7 = view.findViewById(R.id.os7);
        os8 = view.findViewById(R.id.os8);
        os9 = view.findViewById(R.id.os9);
        os10 = view.findViewById(R.id.os10);
        os11 = view.findViewById(R.id.os11);
        os12 = view.findViewById(R.id.os12);
        os13 = view.findViewById(R.id.os13);
        os14 = view.findViewById(R.id.os14);
        os15 = view.findViewById(R.id.os15);
        os16 = view.findViewById(R.id.os16);

        ot1 = view.findViewById(R.id.ot1);
        ot2 = view.findViewById(R.id.ot2);
        ot3 = view.findViewById(R.id.ot3);
        ot4 = view.findViewById(R.id.ot4);
        ot5 = view.findViewById(R.id.ot5);
        ot6 = view.findViewById(R.id.ot6);
        ot7 = view.findViewById(R.id.ot7);
        ot8 = view.findViewById(R.id.ot8);
        ot9 = view.findViewById(R.id.ot9);
        ot10 = view.findViewById(R.id.ot10);
        ot11 = view.findViewById(R.id.ot11);
        ot12 = view.findViewById(R.id.ot12);
        ot13 = view.findViewById(R.id.ot13);
        ot14 = view.findViewById(R.id.ot14);
        ot15 = view.findViewById(R.id.ot15);
        ot16 = view.findViewById(R.id.ot16);

        ps1 = view.findViewById(R.id.ps1);
        ps2 = view.findViewById(R.id.ps2);
        ps3 = view.findViewById(R.id.ps3);
        ps4 = view.findViewById(R.id.ps4);
        ps5 = view.findViewById(R.id.ps5);
        ps6 = view.findViewById(R.id.ps6);
        ps7 = view.findViewById(R.id.ps7);
        ps8 = view.findViewById(R.id.ps8);
        ps9 = view.findViewById(R.id.ps9);
        ps10 = view.findViewById(R.id.ps10);
        ps11 = view.findViewById(R.id.ps11);
        ps12 = view.findViewById(R.id.ps12);
        ps13 = view.findViewById(R.id.ps13);
        ps14 = view.findViewById(R.id.ps14);
        ps15 = view.findViewById(R.id.ps15);
        ps16 = view.findViewById(R.id.ps16);

        pt1 = view.findViewById(R.id.pt1);
        pt2 = view.findViewById(R.id.pt2);
        pt3 = view.findViewById(R.id.pt3);
        pt4 = view.findViewById(R.id.pt4);
        pt5 = view.findViewById(R.id.pt5);
        pt6 = view.findViewById(R.id.pt6);
        pt7 = view.findViewById(R.id.pt7);
        pt8 = view.findViewById(R.id.pt8);
        pt9 = view.findViewById(R.id.pt9);
        pt10 = view.findViewById(R.id.pt10);
        pt11 = view.findViewById(R.id.pt11);
        pt12 = view.findViewById(R.id.pt12);
        pt13 = view.findViewById(R.id.pt13);
        pt14 = view.findViewById(R.id.pt14);
        pt15 = view.findViewById(R.id.pt15);
        pt16 = view.findViewById(R.id.pt16);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
