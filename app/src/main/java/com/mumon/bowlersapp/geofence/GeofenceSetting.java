package com.mumon.bowlersapp.geofence;

public class GeofenceSetting {

    private double latitude;
    private double longitude;
    private float radius;

    public GeofenceSetting(){

    }

    public GeofenceSetting(double latitude, double longitude, float radius) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getRadius() {
        return radius;
    }



}
