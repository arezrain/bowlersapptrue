package com.mumon.bowlersapp;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mumon.bowlersapp.generator.KeywordGenerator;
import com.mumon.bowlersapp.geofence.GeofenceSetting;
import com.mumon.bowlersapp.model.Users;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private Button signup, datePickerYes, datePickerNo;
    private EditText name, password,email, phone;
    private TextView dob;
    private Spinner role;
    private Users user;
    private FirebaseAuth mAuth;
    private ProgressBar pb;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseFirestore db1 = FirebaseFirestore.getInstance();
    private GenerateTeamKey generateTeamKey = new GenerateTeamKey();
    private CardView datePickerCard;
    private DatePicker datePicker;
    private LinearLayout datePickerContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);

        SharedPreferences sharedPreferences = getSharedPreferences("user_data",0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        mAuth = FirebaseAuth.getInstance();

        datePickerCard = (CardView)findViewById(R.id.sign_up_date_picker_card);
        datePickerCard.setVisibility(View.GONE);
        datePickerContainer = findViewById(R.id.sign_up_date_picker_container);
        datePickerContainer.setVisibility(View.GONE);

        datePicker = (DatePicker)findViewById(R.id.sign_up_date_picker);

        datePickerYes = (Button)findViewById(R.id.sign_up_calendar_yes_button);
        datePickerNo = (Button)findViewById(R.id.sign_up_calendar_no_button);

        name = (EditText)findViewById(R.id.name_reg);
        password = (EditText)findViewById(R.id.password_reg);
        email = (EditText)findViewById(R.id.email_reg);
        signup = (Button)findViewById(R.id.signUpButton);
        role = (Spinner)findViewById(R.id.spinnerRole);
        phone = (EditText)findViewById(R.id.phone_reg);
        dob = (TextView)findViewById(R.id.age_reg);
        user = new Users();
        pb = (ProgressBar)findViewById(R.id.progressBar);

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerContainer.setVisibility(View.VISIBLE);
                datePickerCard.setVisibility(View.VISIBLE);
                name.setEnabled(false);
                password.setEnabled(false);
                email.setEnabled(false);
                signup.setEnabled(false);
                role.setEnabled(false);
                phone.setEnabled(false);
                dob.setEnabled(false);
            }
        });

        datePickerYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int monthCal = datePicker.getMonth() + 1;
                String monthText = String.valueOf(monthCal);
                dob.setText(datePicker.getDayOfMonth()+"/"+(monthText)+"/"+datePicker.getYear());
                datePickerContainer.setVisibility(View.GONE);
                datePickerCard.setVisibility(View.GONE);
                name.setEnabled(true);
                password.setEnabled(true);
                email.setEnabled(true);
                signup.setEnabled(true);
                role.setEnabled(true);
                phone.setEnabled(true);
                dob.setEnabled(true);
            }
        });

        datePickerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerContainer.setVisibility(View.GONE);
                datePickerCard.setVisibility(View.GONE);
                name.setEnabled(true);
                password.setEnabled(true);
                email.setEnabled(true);
                signup.setEnabled(true);
                role.setEnabled(true);
                phone.setEnabled(true);
                dob.setEnabled(true);
            }
        });

        datePickerContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerContainer.setVisibility(View.GONE);
                datePickerCard.setVisibility(View.GONE);
                name.setEnabled(true);
                password.setEnabled(true);
                email.setEnabled(true);
                signup.setEnabled(true);
                role.setEnabled(true);
                phone.setEnabled(true);
                dob.setEnabled(true);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pb.setVisibility(View.VISIBLE);

                final String nme = name.getText().toString();
                final String pss = password.getText().toString().trim();
                final String rol = role.getSelectedItem().toString();
                final String eml = email.getText().toString().trim();
                String phn = phone.getText().toString();
                String key = null;
                String DOB = dob.getText().toString();

                final Map<String, Object> docData = new HashMap<>();

                if(rol.equals("Coach")){
                    key = generateTeamKey.getResult();
                }
                docData.put("name", nme);
                docData.put("birthdate", DOB);
                docData.put("phone_no", phn);
                docData.put("email",eml);
                docData.put("role", rol);
                docData.put("team_key", key);

                System.out.println("output check role selected : "+rol);

                if (!nme.isEmpty() && !pss.isEmpty() && !eml.isEmpty() && !phn.isEmpty() && !DOB.isEmpty()){
                    final String finalKey = key;
                    mAuth.createUserWithEmailAndPassword(eml,pss)
                            .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        Log.d("SignUp", "createUserWithEmail:success");

                                        final FirebaseUser user = mAuth.getCurrentUser();

                                        Toast.makeText(SignUpActivity.this, "Successfully sign up \nWelcome "+rol+" "+nme,Toast.LENGTH_SHORT).show();


                                        db.document("users/"+user.getUid())
                                                .set(docData)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        KeywordGenerator keywordGenerator = new KeywordGenerator();
                                                        db.collection("users").document(user.getUid()).update("keyword",keywordGenerator.getGeneratedKeyword(nme));
                                                        if (role.getSelectedItem().toString().equals("Coach")){
                                                            System.out.println("it run this");
                                                            Map<String,Object> team = new HashMap<>();
                                                            team.put("coach",user.getUid());
                                                            db.collection("teams").document(finalKey).set(team);
//                                                        Intent intent = new Intent(SignUpActivity.this, CoachActivity.class);
//                                                        startActivity(intent);

                                                            GeofenceSetting geofenceSetting = new GeofenceSetting(0, 0, 0);
                                                            FirebaseDatabase.getInstance().getReference("GeofenceSetting/"+finalKey).setValue(geofenceSetting);

                                                            mAuth.signOut();
                                                            finish();
                                                        }
                                                        else if (rol.equals("Athlete")){
                                                            mAuth.signOut();
                                                            finish();
                                                        }
                                                        Log.d("TAG", "DocumentSnapshot successfully written!");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w("TAG", "Error writing document", e);
                                                    }
                                                });

                                        pb.setVisibility(View.GONE);

                                        System.out.println("check role : "+docData.get("role"));


                                    }
                                    else {
                                        Log.w("Failed", "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(SignUpActivity.this, "Sign Up Error",Toast.LENGTH_SHORT).show();
                                        pb.setVisibility(View.GONE);
                                    }

                                }
                            });
                }
                else
                {
                    Toast.makeText(SignUpActivity.this,"Please input all the field required!",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    @Override
    public void onBackPressed() {

        if (datePickerContainer.getVisibility() == View.VISIBLE){
            datePickerContainer.setVisibility(View.GONE);
            datePickerCard.setVisibility(View.GONE);
            name.setEnabled(true);
            password.setEnabled(true);
            email.setEnabled(true);
            signup.setEnabled(true);
            role.setEnabled(true);
            phone.setEnabled(true);
            dob.setEnabled(true);
        }
        else {
            super.onBackPressed();
        }
    }
}
