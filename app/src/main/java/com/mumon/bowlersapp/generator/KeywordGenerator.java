package com.mumon.bowlersapp.generator;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class KeywordGenerator {

    private String name;

    public void KeywordGenerator(){
        String name = "";
    }

    public void KeywordGenerator(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getGeneratedKeyword(String name){
        String lowerName = name.toLowerCase();
        int totalname = 0;
        int nameLength = name.length();
        ArrayList<String> keyword = new ArrayList<>();
        ArrayList<String> singleName = new ArrayList<>();

        StringTokenizer st = new StringTokenizer(name," ");

        while (st.hasMoreTokens()){
            singleName.add(st.nextToken());
            totalname =+ 1;
        }



        for (int i = 1; i <= nameLength; i++ ){
            keyword.add(lowerName.substring(0,i));
        }

        for (int i = nameLength-1; i >= 0; i--){
            keyword.add(lowerName.substring(i,nameLength));
        }

        for (int i = 1 ; i < singleName.size(); i++){
            for (int j = 1; j <= singleName.get(i).length(); j++){
                keyword.add(singleName.get(i).substring(0,j));
            }
        }

        System.out.println(singleName);

        return keyword;
    }
}
