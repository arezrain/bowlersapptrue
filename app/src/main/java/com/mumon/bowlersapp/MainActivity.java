package com.mumon.bowlersapp;

import android.app.ActivityOptions;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.util.Pair;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mumon.bowlersapp.athlete.NewAthleteActivity;
import com.mumon.bowlersapp.coach.CoachActivity;
import com.mumon.bowlersapp.coach.CoachMainActivity;
import com.mumon.bowlersapp.model.Users;

public class MainActivity extends AppCompatActivity {


    private FirebaseDatabase firebaseDatabase;
    private FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private Users user;
    private FirebaseAuth mAuth;
    private ProgressBar pb;
    private GenerateTeamKey generateTeamKey = new GenerateTeamKey();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button loginBtn = findViewById(R.id.login_button);
        final TextView title = findViewById(R.id.textView2);
        final EditText email = findViewById(R.id.email);
        final EditText password = findViewById(R.id.password);
        final TextView register = findViewById(R.id.register);

        pb = findViewById(R.id.progressBar2);
        mAuth = FirebaseAuth.getInstance();


        SharedPreferences sharedPreferences = getSharedPreferences("user_data", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            if (sharedPreferences.getString("role", "").equals("Coach")) {
                Intent intent = new Intent(MainActivity.this, CoachMainActivity.class);
                startActivity(intent);
                finish();
            } else if (sharedPreferences.getString("role", "").equals("Athlete") && sharedPreferences.getString("team_key", "").equals("no key")) {
                Intent intent = new Intent(MainActivity.this, NoTeamKeyActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(MainActivity.this, NewAthleteActivity.class);
                startActivity(intent);
                finish();
            }
        }


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String emailText = email.getText().toString();
                final String passwordText = password.getText().toString();
                String key = generateTeamKey.getResult();

                final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("users");

                if (!emailText.isEmpty() && !passwordText.isEmpty()) {
                    pb.setVisibility(View.VISIBLE);
                    email.setEnabled(false);
                    password.setEnabled(false);
                    loginBtn.setEnabled(false);

                    mAuth.signInWithEmailAndPassword(emailText, passwordText)
                            .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d("TAG", "signInWithEmail:success");
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        checkRole(user);
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        pb.setVisibility(View.GONE);
                                        Log.w("TAG", "signInWithEmail:failure", task.getException());
                                        Toast.makeText(MainActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                        email.setEnabled(true);
                                        password.setEnabled(true);
                                        loginBtn.setEnabled(true);
                                    }

                                    // ...
                                }
                            });

//                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//
//
//                            if (dataSnapshot.child(emailText).exists()){
//                                String getuser =(String) dataSnapshot.child(""+emailText+"/email").getValue();
//                                String getpass = (String) dataSnapshot.child(""+emailText+"/password").getValue();
//                                String getrole = (String) dataSnapshot.child(""+emailText+"/role").getValue();
//
//                                if (getuser.equals(emailText) && getpass.equals(passwordText)){
//
//                                    if (getrole.equals("Coach")) {
//                                        pb.setVisibility(View.GONE);
//                                        Intent intent = new Intent(MainActivity.this, CoachActivity.class);
//                                        startActivity(intent);
//                                        email.setEnabled(true);
//                                        password.setEnabled(true);
//                                        loginBtn.setEnabled(true);
//                                        Toast.makeText(getApplicationContext(), "Welcome Coach!", Toast.LENGTH_SHORT).show();
//
//                                    }
//                                    else if (getrole.equals("Athlete")) {
//                                        pb.setVisibility(View.GONE);
//                                        Intent intent = new Intent(MainActivity.this, athleteActivity.class);
//                                        startActivity(intent);
//                                        email.setEnabled(true);
//                                        password.setEnabled(true);
//                                        loginBtn.setEnabled(true);
//                                        Toast.makeText(getApplicationContext(), "Welcome Athlete!!", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                                else {
//                                    email.setEnabled(true);
//                                    password.setEnabled(true);
//                                    loginBtn.setEnabled(true);
//                                    pb.setVisibility(View.GONE);
//                                    Toast.makeText(getApplicationContext(),"Invalid username or password!",Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                            else {
//                                email.setEnabled(true);
//                                password.setEnabled(true);
//                                loginBtn.setEnabled(true);
//                                pb.setVisibility(View.GONE);
//                                Toast.makeText(getApplicationContext(), "Invalid username or password!", Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });
                } else if (emailText.isEmpty() && passwordText.isEmpty())
                    Toast.makeText(getApplicationContext(), "Please input username and password", Toast.LENGTH_SHORT).show();

                else if (!emailText.isEmpty() && passwordText.isEmpty())
                    Toast.makeText(getApplicationContext(), "Please input password", Toast.LENGTH_SHORT).show();

                else if (emailText.isEmpty() && !passwordText.isEmpty())
                    Toast.makeText(getApplicationContext(), "Please input username", Toast.LENGTH_SHORT).show();

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click register");
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);

                Pair[] pairs = new Pair[3];
                //pairs[0] = new Pair<View, String>(title, "titleTransition");
                pairs[0] = new Pair<View, String>(email, "emailTransition");
                pairs[1] = new Pair<View, String>(password, "passwordTransition");
                pairs[2] = new Pair<View, String>(loginBtn, "buttonTransition");

                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent, options.toBundle());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser currUser = mAuth.getCurrentUser();

    }

    public void checkRole(final FirebaseUser user) {
        final String email = user.getEmail();

        DocumentReference docRef = firebaseFirestore.document("users/" + user.getUid());

//        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                task.getResult().getString("role");
//                Toast.makeText(getApplicationContext(),"Successfully get "+task.getResult().getString("role"),Toast.LENGTH_SHORT).show();
//            }
//        });


        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {

                    String role = documentSnapshot.getString("role");
                    String name = documentSnapshot.getString("name");
                    String key = documentSnapshot.getString("team_key");

                    SharedPreferences sharedPreferences = getSharedPreferences("user_data", 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString("role", role);
                    editor.putString("team_key", key);
                    editor.putString("name", name);

                    if (role.equals("Coach")) {

                        pb.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Welcome coach " + documentSnapshot.getString("name"), Toast
                                .LENGTH_SHORT).show();
//                        editor.putBoolean("logged_in", true);

                        Intent intent = new Intent(MainActivity.this, CoachMainActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (role.equals("Athlete") && key != null) {

                        pb.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Welcome athlete " + documentSnapshot.getString("name"), Toast
                                .LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, NewAthleteActivity.class);
                        startActivity(intent);
                        finish();

                    } else {

                        pb.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Welcome athlete " + documentSnapshot.getString("name"), Toast
                                .LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, NoTeamKeyActivity.class);
                        editor.putString("team_key", "no key");
                        startActivity(intent);
                        finish();

                    }

                    editor.apply();

                } else
                    pb.setVisibility(View.GONE);
                //Toast.makeText(getApplicationContext(),"No role found",Toast.LENGTH_SHORT).show();

            }
        });

    }

}
