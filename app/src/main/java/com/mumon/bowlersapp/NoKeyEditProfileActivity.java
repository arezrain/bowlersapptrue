package com.mumon.bowlersapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mumon.bowlersapp.generator.KeywordGenerator;
import com.mumon.bowlersapp.model.Users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NoKeyEditProfileActivity extends AppCompatActivity {

    Window window;
    private final int PICK_IMAGE_REQUEST = 22;
    private Button male, female, edit, cancel, save, editProfilPic;
    private EditText name, phone, birthdate;
    private TextView drawerName;
    private RelativeLayout saveContainer;
    private String genderText, cancelGender;
    private ImageView profilePic, drawerProfilePic;
    private Uri selectedImageUri;
    Users users = new Users();
    AlertDialog loading;
    StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_key_edit_profile);

        window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));

        male = findViewById(R.id.athlete_profile_male);
        female = findViewById(R.id.athlete_profile_female);
        edit = findViewById(R.id.athlete_profile_edit);
        save = findViewById(R.id.athlete_profile_save);
        cancel = findViewById(R.id.athlete_profile_cancel);
        saveContainer = findViewById(R.id.athlete_profile_save_container);
        name = findViewById(R.id.athlete_profile_name);
        phone = findViewById(R.id.athlete_profile_phone);
        birthdate = findViewById(R.id.athlete_profile_birthdate);
        profilePic = findViewById(R.id.athlete_profile_img);
        editProfilPic = findViewById(R.id.athlete_profile_edit_profile_pic);
        final TransitionDrawable transitionDrawable = (TransitionDrawable) cancel.getBackground();

        SetLoadingDialog();

        editProfilPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectImage();
            }
        });


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setVisibility(View.GONE);
                save.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.VISIBLE);

                male.setEnabled(true);
                female.setEnabled(true);
                name.setEnabled(true);
                phone.setEnabled(true);
                birthdate.setEnabled(true);

                transitionDrawable.startTransition(300);
                cancel.animate().translationX(-300);

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (name.getText().toString().trim().equals(users.getName()) &&
                        genderText.equals(users.getGender()) &&
                        phone.getText().toString().trim().equals(users.getPhone_no()) &&
                        birthdate.getText().toString().trim().equals(users.getBirthdate())){

                    Toast.makeText(NoKeyEditProfileActivity.this, "No changes made", Toast.LENGTH_SHORT).show();
                }
                else {

                    loading.show();

                    Map<String,Object> userUpdate = new HashMap<>();

                    userUpdate.put("name",name.getText().toString().trim());
                    userUpdate.put("gender",genderText);
                    userUpdate.put("phone_no",phone.getText().toString().trim());
                    userUpdate.put("birthdate",birthdate.getText().toString().trim());

                    FirebaseFirestore.getInstance().document("users/"+
                            FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .update(userUpdate).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            loading.dismiss();
                            edit.setVisibility(View.VISIBLE);
                            transitionDrawable.reverseTransition(300);
                            cancel.animate().translationX(3).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(NoKeyEditProfileActivity.this, "Successfully updated profile", Toast.LENGTH_SHORT).show();
                                    edit.setVisibility(View.VISIBLE);

                                    male.setEnabled(false);
                                    female.setEnabled(false);
                                    name.setEnabled(false);
                                    phone.setEnabled(false);
                                    birthdate.setEnabled(false);

                                    users.setName(name.getText().toString().trim());
                                    users.setPhone_no(phone.getText().toString().trim());
                                    users.setBirthdate(birthdate.getText().toString().trim());
                                    users.setGender(genderText);

                                    drawerName.setText(users.getName());

                                    cancelGender = genderText;
                                    KeywordGenerator keywordGenerator = new KeywordGenerator();
                                    ArrayList<String> keyword = keywordGenerator.getGeneratedKeyword(name.getText().toString().trim());
                                    FirebaseFirestore.getInstance().document("users/"+
                                            FirebaseAuth.getInstance().getCurrentUser().getUid()).update("keyword",keyword);

                                }
                            });
                        }
                    }).addOnCanceledListener(new OnCanceledListener() {
                        @Override
                        public void onCanceled() {
                            loading.dismiss();
                            Toast.makeText(NoKeyEditProfileActivity.this, "Unable to update profile", Toast.LENGTH_SHORT).show();
                        }
                    });

                }


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edit.setVisibility(View.VISIBLE);
                transitionDrawable.reverseTransition(300);
                cancel.animate().translationX(3).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        cancel.setVisibility(View.GONE);
                        save.setVisibility(View.GONE);

                        male.setEnabled(false);
                        female.setEnabled(false);
                        name.setEnabled(false);
                        phone.setEnabled(false);
                        birthdate.setEnabled(false);

                        name.setText(users.getName());
                        phone.setText(users.getPhone_no());
                        birthdate.setText(users.getBirthdate());

                        if (cancelGender.equals("m")){
                            isMale();
                        }
                        else if (cancelGender.equals("f")){
                            isFemale();
                        }
                    }
                });

            }
        });

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isMale();

                genderText = "m";

                System.out.println(genderText);
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFemale();

                genderText = "f";

                System.out.println(genderText);
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        fetchUserData();
    }

    private void isFemale() {
        female.setBackground(getResources().getDrawable(R.drawable.custom_profile_selected));
        female.setTextColor(Color.WHITE);

        male.setBackground(getResources().getDrawable(R.drawable.custom_field_profile));
        male.setTextColor(Color.rgb(40,64,95));
    }

    private void isMale() {
        male.setBackground(getResources().getDrawable(R.drawable.custom_profile_selected));
        male.setTextColor(Color.WHITE);

        female.setBackground(getResources().getDrawable(R.drawable.custom_field_profile));
        female.setTextColor(Color.rgb(40,64,95));
    }

    private void fetchUserData() {

        FirebaseFirestore.getInstance().document("users/"+ FirebaseAuth.getInstance().getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        users = documentSnapshot.toObject(Users.class);
//                        String defGender = "m";
                        name.setText(users.getName());
                        phone.setText(users.getPhone_no());
                        birthdate.setText(users.getBirthdate());
                        if (users.getGender() != null){

                            if (users.getGender().equals("m")){
                                isMale();
                                genderText = "m";
                                cancelGender = "m";
                            }
                            else if (users.getGender().equals("f")){
                                isFemale();
                                genderText = "f";
                                cancelGender = "f";
                            }
                        }
                        else{
                            isMale();
                            genderText = "m";
                            cancelGender = "m";
                        }


                        System.out.println(genderText);

                        if (users.getImage_uri() != null)
                            Glide.with(NoKeyEditProfileActivity.this)
                                    .load(users.getImage_uri())
                                    .circleCrop()
                                    .into(profilePic);

                        else {
                            profilePic.setImageURI(null);
                        }

                    }
                });

    }

    public void SetLoadingDialog(){

        ViewGroup viewGroup = findViewById(android.R.id.content);

        View loadingView = LayoutInflater.from(NoKeyEditProfileActivity.this).inflate(R.layout.dialog_loading,viewGroup,false);

        AlertDialog.Builder builder = new AlertDialog.Builder(NoKeyEditProfileActivity.this);

        builder.setView(loadingView);

        loading = builder.create();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.logout:
                Toast.makeText(NoKeyEditProfileActivity.this, "logout selected", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }



    private void SelectImage()
    {

        // Defining Implicit Intent to mobile gallery
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(
                        intent,
                        "Select Image from here..."),
                PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST
                && resultCode == RESULT_OK
                && data != null
                && data.getData() != null) {

            // Get the Uri of data

            selectedImageUri = data.getData();


        }

        uploadImage();
    }

    private void uploadImage()
    {
        if (selectedImageUri != null) {

            // Code for showing progressDialog while uploading
            final ProgressDialog progressDialog
                    = new ProgressDialog(NoKeyEditProfileActivity.this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            // Defining the child of storageReference
            final StorageReference ref = mStorageRef.child("profile_pics/" + FirebaseAuth.getInstance().getCurrentUser().getUid());

            // adding listeners on upload
            // or failure of image
            ref.putFile(selectedImageUri)
                    .addOnSuccessListener(
                            new OnSuccessListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onSuccess(
                                        UploadTask.TaskSnapshot taskSnapshot)
                                {

                                    // Image uploaded successfully
                                    // Dismiss dialog
                                    progressDialog.dismiss();
                                    Toast
                                            .makeText(NoKeyEditProfileActivity.this,
                                                    "Image Uploaded!!",
                                                    Toast.LENGTH_SHORT)
                                            .show();

                                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(final Uri uri) {
                                            Map<String, Object> updatedImageUri = new HashMap<>();
                                            updatedImageUri.put("image_uri",uri.toString());
                                            FirebaseFirestore.getInstance().document("users/"+
                                                    FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                    .update(updatedImageUri).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Glide.with(NoKeyEditProfileActivity.this).load(uri).circleCrop().into(profilePic);
                                                    Glide.with(NoKeyEditProfileActivity.this).load(uri).circleCrop().into(drawerProfilePic);
                                                }
                                            });
                                        }
                                    });
                                }
                            })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {

                            // Error, Image not uploaded
                            progressDialog.dismiss();
                            Toast
                                    .makeText(NoKeyEditProfileActivity.this,
                                            "Failed " + e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                    .show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                // Progress Listener for loading
                                // percentage on the dialog box
                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot)
                                {
                                    double progress
                                            = (100.0
                                            * taskSnapshot.getBytesTransferred()
                                            / taskSnapshot.getTotalByteCount());
                                    progressDialog.setMessage(
                                            "Uploaded "
                                                    + (int)progress + "%");
                                }
                            });
        }
    }
}
