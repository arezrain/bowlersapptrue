package com.mumon.bowlersapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class NoTeamKeyActivity extends AppCompatActivity {

    private Button logout, enter, editProfile;
    private EditText entered_key;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_team_key);

        SharedPreferences sharedPreferences = getSharedPreferences("user_data",0);
        final String name = sharedPreferences.getString("name", "");

        logout = (Button)findViewById(R.id.nokeylogout);
        enter = (Button)findViewById(R.id.register_team_key);
        entered_key = (EditText)findViewById(R.id.entered_team_key);
        editProfile = findViewById(R.id.nokeyeditprogile);

        final Map<String, Object> requestData = new HashMap<>();

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String key = entered_key.getText().toString().trim();

                requestData.put("user_id",mAuth.getCurrentUser());
                requestData.put("user_name", name);
                db.document("users/"+user.getUid()).update("team_key",null);
                db.document("users/"+user.getUid()).update("request_key",key);
                Toast.makeText(NoTeamKeyActivity.this,"Succesfully request to join the team, please wait for the approval by coach",Toast.LENGTH_SHORT).show();
                entered_key.setText("");

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAuth.signOut();
                SharedPreferences sharedPreferences = getSharedPreferences("user_data",0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("role",null);
                editor.putString("team_key",null);
                editor.putString("name",null);
                editor.apply();
                Intent intent = new Intent(NoTeamKeyActivity.this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Successfully Logout", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NoTeamKeyActivity.this, NoKeyEditProfileActivity.class);
                startActivity(intent);
            }
        });
    }
}
