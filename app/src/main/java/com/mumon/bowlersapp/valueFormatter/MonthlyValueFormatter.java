package com.mumon.bowlersapp.valueFormatter;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

public class MonthlyValueFormatter extends ValueFormatter {

    private final BarLineChartBase<?> chart;

    public MonthlyValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value) {

        int weekIndex = (int) value;

        if (weekIndex == 1)
            return "Jan";
        else if (weekIndex == 2)
            return "Feb";
        else if (weekIndex == 3)
            return "Mar";
        else if (weekIndex == 4)
            return "Apr";
        else if (weekIndex == 5)
            return "May";
        else if (weekIndex == 6)
            return "Jun";
        else if (weekIndex == 7)
            return "Jul";
        else if (weekIndex == 8)
            return "Aug";
        else if (weekIndex == 9)
            return "Sep";
        else if (weekIndex == 10)
            return "Oct";
        else if (weekIndex == 11)
            return "Nov";
        else if (weekIndex == 12)
            return "Dec";
        else
            return "";

    }

}
