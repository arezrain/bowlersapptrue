package com.mumon.bowlersapp.valueFormatter;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

/**
 * Created by mumon on 02/06/19.
 */
public class WeeklyValueFormatter extends ValueFormatter
{

    private final BarLineChartBase<?> chart;

    public WeeklyValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value) {

        int weekIndex = (int) value;

        if (weekIndex == 1)
            return "Week 1";
        else if (weekIndex == 2)
            return "Week 2";
        else if (weekIndex == 3)
            return "Week 3";
        else if (weekIndex == 4)
            return "Week 4";
        else if (weekIndex == 5)
            return "Week 5";
        else
            return "";

    }


}
