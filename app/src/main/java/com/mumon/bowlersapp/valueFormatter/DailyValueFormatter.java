package com.mumon.bowlersapp.valueFormatter;

import android.widget.Switch;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

/**
 * Created by philipp on 02/06/16.
 */
public class DailyValueFormatter extends ValueFormatter
{

    private final BarLineChartBase<?> chart;

    public DailyValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value) {


        if (value == 1 || value == 21 || value == 31){
            return (int)value+"st";
        }
        else if (value == 2 || value == 22)
        {
            return (int)value+"nd";
        }
        else if (value == 3 || value == 23){
            return (int)value+"rd";
        }
        else if (value - (int)value == 0 && (value > 0 && value < 32)){
            return (int)value+"th";
        }
        else
            return "";

//        if (value == 1) {
//            return "1st";
//        } else if (value == 2) {
//            return "2nd";
//        } else if (value == 3) {
//            return "3rd";
//        } else if (value == 4) {
//            return "4";
//        } else if (value == 5) {
//            return "5";
//        } else if (value == 6) {
//            return "6";
//        } else if (value == 7) {
//            return "7";
//        } else if (value == 8) {
//            return "8";
//        } else if (value == 9) {
//            return "9";
//        } else if (value == 10) {
//            return "10";
//        } else if (value == 11) {
//            return "11";
//        } else if (value == 12) {
//            return "12";
//        } else if (value == 13) {
//            return "13";
//        } else if (value == 14) {
//            return "14";
//        } else if (value == 15) {
//            return "15";
//        } else if (value == 16) {
//            return "16";
//        } else if (value == 17) {
//            return "17";
//        } else if (value == 18) {
//            return "18";
//        } else if (value == 19) {
//            return "19";
//        } else if (value == 20) {
//            return "20";
//        } else if (value == 21) {
//            return "21";
//        } else if (value == 22) {
//            return "22";
//        } else if (value == 23) {
//            return "23";
//        } else if (value == 24) {
//            return "24";
//        } else if (value == 25) {
//            return "25";
//        } else if (value == 26) {
//            return "26";
//        } else if (value == 27) {
//            return "27";
//        } else if (value == 28) {
//            return "28";
//        } else if (value == 29) {
//            return "29";
//        } else if (value == 30) {
//            return "30";
//        } else if (value == 31) {
//            return "31";
//        }else
//            return "";

    }


}
