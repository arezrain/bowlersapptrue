package com.mumon.bowlersapp.coach;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.adapter.FeedbackAdapter;
import com.mumon.bowlersapp.adapter.LogAdapter;
import com.mumon.bowlersapp.model.Feedback;
import com.mumon.bowlersapp.model.LogModel;

import java.util.Date;

public class CoachTrainingLog extends Fragment {

    LogAdapter mAdapter;
    LinearLayoutManager linearLayoutManager;
    FloatingActionButton addNote;
    AlertDialog addNoteAlert, editNoteAlert;
    Button save, cancel;
    Window window;
    Toolbar toolbar;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.athlete_training_note, container, false);

        window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        addNote = view.findViewById(R.id.add_training_note);


        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNoteAlert.show();



            }
        });
        setUpRecyclerView(view);
        setUpAddNote(view);

        return view;
    }

    private void setUpAddNote(View view) {
        ViewGroup  viewGroup = view.findViewById(android.R.id.content);

        View logView = LayoutInflater.from(getContext()).inflate(R.layout.add_log,viewGroup,false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        final EditText getTitle, note;
        save = logView.findViewById(R.id.save_log);
        cancel = logView.findViewById(R.id.cancel_log);
        getTitle = logView.findViewById(R.id.title_log);
        note = logView.findViewById(R.id.text_log);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getTitle.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Please input the title", Toast.LENGTH_SHORT).show();
                }
                else if (note.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Please input the notes", Toast.LENGTH_SHORT).show();
                }
                else {

                    Date date = new Date();
                    String dateText = (String) android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", date);
                    String day = dateText.substring(8,10);
                    String month = dateText.substring(5,7);
                    String year = dateText.substring(0,4);
                    String time = dateText.substring(11,19);
                    String log = note.getText().toString().trim();
                    String title = getTitle.getText().toString().trim();
                    LogModel logModel = new LogModel(time,day,title,log,month,year);

                    FirebaseFirestore.getInstance().document("users/"+
                            FirebaseAuth.getInstance().getCurrentUser().getUid()+"/log/"+logModel.getTime()+
                            logModel.getDay()+logModel.getMonth()+logModel.getYear()).set(logModel)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    addNoteAlert.dismiss();
                                }
                            });

                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNoteAlert.dismiss();
            }
        });


        builder.setView(logView);

        addNoteAlert = builder.create();

        addNoteAlert.setCancelable(false);





    }

    private void setUpEditNote(View view, LogModel logModel, final String id) {
        ViewGroup  viewGroup = view.findViewById(android.R.id.content);

        View logView = LayoutInflater.from(getContext()).inflate(R.layout.add_log,viewGroup,false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        final EditText getTitle, note;
        save = logView.findViewById(R.id.save_log);
        cancel = logView.findViewById(R.id.cancel_log);
        getTitle = logView.findViewById(R.id.title_log);
        note = logView.findViewById(R.id.text_log);

        getTitle.setText(logModel.getTitle());
        note.setText(logModel.getLogs());

        final LogModel finalLog = logModel;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getTitle.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Please input the title", Toast.LENGTH_SHORT).show();
                }
                else if (note.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Please input the notes", Toast.LENGTH_SHORT).show();
                }
                else {

                    finalLog.setTitle(getTitle.getText().toString());
                    finalLog.setLogs(note.getText().toString());


                    FirebaseFirestore.getInstance().document("users/"+
                            FirebaseAuth.getInstance().getCurrentUser().getUid()+"/log/"+id).set(finalLog)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    editNoteAlert.dismiss();
                                }
                            });

                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editNoteAlert.dismiss();
            }
        });


        builder.setView(logView);

        editNoteAlert = builder.create();

        editNoteAlert.setCancelable(false);





    }

    private void setUpRecyclerView(View view){

        System.out.println("test comment");
        Query query = FirebaseFirestore.getInstance().collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("log").orderBy("year").orderBy("month").orderBy("day")
                .orderBy("time");

        FirestoreRecyclerOptions<LogModel> recyclerOptions = new FirestoreRecyclerOptions.Builder<LogModel>()
                .setQuery(query,LogModel.class)
                .build();

        mAdapter = new LogAdapter(recyclerOptions);

        RecyclerView recyclerView = view.findViewById(R.id.training_note_recycler);
//        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
//        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.scrollToPosition(mAdapter.getItemCount()-1);
        recyclerView.setLayoutManager(linearLayoutManager);


        final View finalView = view;
        mAdapter.setOnItemClickListener(new LogAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                LogModel passLogModel = documentSnapshot.toObject(LogModel.class);
                setUpEditNote(finalView,passLogModel,documentSnapshot.getId());
                editNoteAlert.show();
            }
        });

        mAdapter.setOnItemLongClickListener(new LogAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(DocumentSnapshot documentSnapshot, final int position) {
                Toast.makeText(getActivity(), "test "+documentSnapshot.getId(), Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Do you want to delete this log?");
                builder.setCancelable(true);

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAdapter.deleteMessage(position);
                        dialogInterface.cancel();
                        Toast.makeText(getContext(), "Deleted Comment", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        recyclerView.setAdapter(mAdapter);



    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter.stopListening();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.startListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAdapter.stopListening();
    }
}
