package com.mumon.bowlersapp.coach;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.adapter.RequestAdapterNew;
import com.mumon.bowlersapp.model.RequestItem;
import com.mumon.bowlersapp.adapter.TeamAdapterNew;
import com.mumon.bowlersapp.custom.OwnViewPager;
import com.mumon.bowlersapp.model.Users;

import java.util.ArrayList;

public class FragmentRequestList extends Fragment{

    View view;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference requestRef = db.collection("users");
    private RecyclerView recyclerView;
    private RequestAdapterNew mAdapter;
    private TeamAdapterNew teamAdapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<RequestItem> requestItems = new ArrayList<>();
    ArrayList<String> requestId = new ArrayList<>();
    OwnViewPager ownViewPager;
    Window window;
    Toolbar toolbar;
    EditText search;

    public FragmentRequestList() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.coach_request_list_fragment, container, false);

        window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        search = view.findViewById(R.id.coach_request_list_search);
//        coachBar.setVisibility(View.VISIBLE);

        setUpRecyclerView(view,null);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String check = search.getText().toString().trim();
                if (check.isEmpty()){
                    setUpRecyclerView(view, null);
                    mAdapter.startListening();
                }
                else {
                    setUpRecyclerView(view, search.getText().toString());
                    mAdapter.startListening();
                }
            }
        });

        return view;
    }

    private void setUpRecyclerView(View view, String keyword){


        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data",0);
        Query query;
        if (keyword!=null){
            query = requestRef.whereEqualTo("request_key",sharedPreferences.getString("team_key",""))
                    .whereEqualTo("role","Athlete").whereArrayContains("keyword",keyword.toLowerCase()).orderBy("name");
        }
        else{
            query = requestRef.whereEqualTo("request_key",sharedPreferences.getString("team_key",""))
                    .whereEqualTo("role","Athlete").orderBy("name");
        }


        FirestoreRecyclerOptions<Users> recyclerOptions = new FirestoreRecyclerOptions.Builder<Users>()
                .setQuery(query, Users.class)
                .build();

        mAdapter = new RequestAdapterNew(recyclerOptions);

        RecyclerView recyclerView = view.findViewById(R.id.request_recycler);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                String key = sharedPreferences.getString("team_key","");

                switch (direction){
                    case ItemTouchHelper.LEFT:
                        mAdapter.acceptRequest(viewHolder.getAdapterPosition(),key);
                        Toast.makeText(getContext(),"Request accepted",Toast.LENGTH_SHORT).show();
                        break;

                    case ItemTouchHelper.RIGHT:
                        mAdapter.declineRequest(viewHolder.getAdapterPosition());
                        Toast.makeText(getContext(),"Request declined",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        mAdapter.setOnItemClickListener(new RequestAdapterNew.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                Users user = documentSnapshot.toObject(Users.class);
                String id = documentSnapshot.getId();
                Toast.makeText(getActivity(), "ID : "+id,Toast.LENGTH_SHORT).show();
            }
        });

        mAdapter.setOnAcceptClickListener(new RequestAdapterNew.OnAcceptClickListener() {
            @Override
            public void onAcceptClick(DocumentSnapshot documentSnapshot, int position) {
                String id = documentSnapshot.getId();
                db.document("users/"+id).update("request_key",null);
                db.document("users/"+id).update("team_key"
                        ,sharedPreferences.getString("team_key",""));
            }
        });

        mAdapter.setOnDeclineClickListener(new RequestAdapterNew.OnDeclineClickListener() {
            @Override
            public void onDeclineClick(DocumentSnapshot documentSnapshot, int position) {
                String id = documentSnapshot.getId();
                db.document("users/"+id).update("request_key",null);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}
