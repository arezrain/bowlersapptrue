package com.mumon.bowlersapp.coach;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.mumon.bowlersapp.MainActivity;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.adapter.ViewPagerAdapter;

public class CoachActivity extends AppCompatActivity {

    private Button menu_btn;
    public NavController navController;
    RelativeLayout coachBar;
    CardView radarContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coach_activity);

        coachBar = (RelativeLayout) findViewById(R.id.coach_bar);

        menu_btn = (Button) findViewById(R.id.menu_btn);

        radarContainer = findViewById(R.id.radar_container);

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.animate().rotation(360).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        menu_btn.setRotation(0);
                    }
                });
                PopupMenu popup = new PopupMenu(CoachActivity.this, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.main_menu, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.logout:
                                menu_btn.animate().rotation(-360);
                                FirebaseAuth.getInstance().signOut();
                                SharedPreferences sharedPreferences = getSharedPreferences("user_data",0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("role",null);
                                editor.putString("team_key",null);
                                editor.putString("name",null);
                                editor.apply();
                                Intent intent = new Intent(CoachActivity.this, MainActivity.class);
                                startActivity(intent);
                                Toast.makeText(getApplicationContext(), "Successfully Logout", Toast.LENGTH_SHORT).show();
                                finish();
                                return true;
                        }
                        menu_btn.animate().rotation(-360);
                        return false;

                    }
                });

            }
        });

        setupNavigation();

    }

    private void setupNavigation() {

        navController = Navigation.findNavController(this,R.id.coach_nav_host);

    }

}
