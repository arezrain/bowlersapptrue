package com.mumon.bowlersapp.coach;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mumon.bowlersapp.GeofenceDetails;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.geofence.GeofenceSetting;

import static com.mumon.bowlersapp.util.Constants.MAPVIEW_BUNDLE_KEY;

public class FragmentSettingCoach extends Fragment implements OnMapReadyCallback, LocationListener, GoogleMap.OnMapLongClickListener {

    View view;

    private static final int REQUEST_LOCATION = 1;

    private MapView mapView;
    private GoogleMap mMap;
    private SeekBar seekBar;
    private TextView radiusText, team_key;
    private Button save;
    private Switch aSwitch;
    private EditText inputLat, inputLong;
    private GeofenceDetails geofenceDetails;
    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = firebaseDatabase.getReference("GeofenceSetting");
    private double lat = 0.0, lon = 0.0;
    private Marker locate;
    private Circle circle;
    private LatLng latLng;
    private LocationManager locationManager;
    private Button edit;

    Window window;
    Toolbar toolbar;

    public FragmentSettingCoach() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.coach_setting_fragment, container, false);

        window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        mapView = (MapView) view.findViewById(R.id.mapView);
        seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        radiusText = (TextView) view.findViewById(R.id.radius_text);
        aSwitch = (Switch) view.findViewById(R.id.switchs);
        inputLat = (EditText) view.findViewById(R.id.latitude);
        inputLong = (EditText) view.findViewById(R.id.longitude);
        save = (Button) view.findViewById(R.id.savegeosetting);
        team_key = (TextView)view.findViewById(R.id.team_key);

        edit = view.findViewById(R.id.coach_setting_edit_button);


        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data",0);
        SharedPreferences.Editor editor = sharedPreferences.edit();



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (aSwitch.isEnabled()){
                    aSwitch.setChecked(false);
                    firebaseDatabase.getReference("GeofenceSetting/"+sharedPreferences.getString("team_key",""))
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        GeofenceDetails geofenceDetails = dataSnapshot.getValue(GeofenceDetails.class);

                                        edit.setBackground(getResources().getDrawable(R.drawable.custom_edit_button));
                                        edit.setText("edit");

                                        inputLat.setText(String.valueOf(geofenceDetails.getLatitude()));
                                        inputLong.setText(String.valueOf(geofenceDetails.getLongitude()));
                                        seekBar.setProgress((int) geofenceDetails.getRadius());

                                        inputLat.setEnabled(false);
                                        inputLong.setEnabled(false);
                                        seekBar.setEnabled(false);
                                        save.setVisibility(View.GONE);
                                        aSwitch.setEnabled(false);

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }
                else {
                    edit.setBackground(getResources().getDrawable(R.drawable.custom_edit_cancel));
                    edit.setText("cancel");

                    inputLat.setEnabled(true);
                    inputLong.setEnabled(true);
                    seekBar.setEnabled(true);
                    save.setVisibility(View.VISIBLE);
                    aSwitch.setEnabled(true);
                }


            }
        });

        team_key.setText(sharedPreferences.getString("team_key","Team key"));

        ActivityCompat.requestPermissions(getActivity(), new String[]
                {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);


        initGoogleMap(savedInstanceState);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            onGPS();
        } else {
            getLocation();
            latLng = new LatLng(lat, lon);
        }


        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    inputLat.setEnabled(false);
                    inputLong.setEnabled(false);
                    getLocation();


                    inputLat.setText(String.valueOf(lat));
                    inputLong.setText(String.valueOf(lon));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));
                    circle.setCenter(new LatLng(lat, lon));
                    locate.setVisible(false);
                } else {
                    inputLat.setEnabled(true);
                    inputLong.setEnabled(true);
                }
            }
        });


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radiusText.setText(String.valueOf(progress));
                if (inputLat.getText().toString().trim().length() > 0 && inputLong.getText().toString().trim().length() > 0)
                    circle.setRadius((double) progress);
                else
                    circle.setRadius(0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        inputLat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String check = inputLat.getText().toString().trim();
                if (!check.isEmpty() && !aSwitch.isChecked()) {
                    lat = (Double.valueOf(inputLat.getText().toString().trim()));
                    String latCheck = String.valueOf(lat);
                    String lonCheck = inputLong.getText().toString().trim();
                    if (!latCheck.isEmpty() && !lonCheck.isEmpty()){
                        circle.setCenter(new LatLng(lat, lon));
                        locate.setPosition(new LatLng(lat, lon));
                        locate.setVisible(true);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));
                    }

                } else {
                    locate.setVisible(false);
                }
            }
        });

        inputLong.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String check = inputLong.getText().toString().trim();
                String check2 = inputLat.getText().toString().trim();
                if (!check.isEmpty() && !aSwitch.isChecked() && !check2.isEmpty()) {
                    lon = (Double.valueOf(inputLong.getText().toString().trim()));
                    String lonCheck = String.valueOf(lon);
                    String latCheck = inputLat.getText().toString().trim();
                    if (!lonCheck.isEmpty() && !latCheck.isEmpty()){
                        System.out.println(lat+" "+ lon);
                        circle.setCenter(new LatLng(lat, lon));
                        locate.setPosition(new LatLng(lat, lon));
                        locate.setVisible(true);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));

                    }

                } else {
                    locate.setVisible(false);
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Latitude : " + lat +
                        "\nLongitude : " + lon +
                        "\nRadius : " + seekBar.getProgress(), Toast.LENGTH_LONG).show();

                geofenceDetails = new GeofenceDetails();
                geofenceDetails.setLatitude(lat);
                geofenceDetails.setLongitude(lon);
                geofenceDetails.setRadius(seekBar.getProgress());
                databaseReference.child(sharedPreferences.getString("team_key","no_key")).setValue(geofenceDetails).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        inputLat.setEnabled(false);
                        inputLong.setEnabled(false);
                        seekBar.setEnabled(false);
                        save.setVisibility(View.GONE);
                        aSwitch.setEnabled(false);
                        edit.setBackground(getResources().getDrawable(R.drawable.custom_edit_button));
                        edit.setText("edit");
                        edit.setVisibility(View.VISIBLE);
                    }
                });




            }
        });

        return view;
    }


    private void initGoogleMap(Bundle savedInstanceState) {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {

        mMap = map;

        latLng = new LatLng(lat, lon);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));

        mMap.setOnMapLongClickListener(this);
        circle = mMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(0)
                .strokeColor(Color.BLUE)
                .fillColor(0x220000ff)
                .strokeWidth(5.0f)
        );

        locate = mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)));
        locate.setVisible(false);


        map.setMyLocationEnabled(true);
        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_data",0);

        firebaseDatabase.getReference("GeofenceSetting/"+sharedPreferences.getString("team_key",""))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            GeofenceDetails geofenceDetails = dataSnapshot.getValue(GeofenceDetails.class);

                            edit.setBackground(getResources().getDrawable(R.drawable.custom_edit_button));
                            edit.setText("edit");

                            inputLat.setText(String.valueOf(geofenceDetails.getLatitude()));
                            inputLong.setText(String.valueOf(geofenceDetails.getLongitude()));
                            seekBar.setProgress((int) geofenceDetails.getRadius());

                            inputLat.setEnabled(false);
                            inputLong.setEnabled(false);
                            seekBar.setEnabled(false);
                            save.setVisibility(View.GONE);
                            aSwitch.setEnabled(false);

                        }
                        else {
                            edit.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void onGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.
                checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps != null) {
                lat = LocationGps.getLatitude();
                lon = LocationGps.getLongitude();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        lat = location.getLatitude();
        lon = location.getLongitude();

        if (aSwitch.isChecked() == true) {
            inputLat.setText(Double.toString(location.getLatitude()));
            inputLong.setText(Double.toString(location.getLongitude()));
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    public void onMapLongClick(LatLng point) {

        if (!aSwitch.isChecked() && inputLat.isEnabled()) {
            latLng = point;
            inputLong.setText(Double.toString(latLng.longitude));
            inputLat.setText(Double.toString(latLng.latitude));
            locate.setPosition(latLng);
            locate.setVisible(true);
        }

    }


}
