package com.mumon.bowlersapp.coach;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mumon.bowlersapp.MainActivity;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.Users;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class CoachMainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {
    public Toolbar toolbar;

    public DrawerLayout drawerLayout;

    public NavController navController;

    public NavigationView navigationView;

    private TextView name, email;
    private ImageView profilePic;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coach_main_activity);


        setupNavigation();
        fetchUserData();

    }

    private void fetchUserData() {
        FirebaseFirestore.getInstance().document("users/"+FirebaseAuth.getInstance().getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Users users = new Users();
                        users = documentSnapshot.toObject(Users.class);

                        name.setText(users.getName());
                        email.setText(users.getEmail());
                        Glide.with(CoachMainActivity.this)
                                .load(users.getImage_uri())
                                .circleCrop()
                                .into(profilePic);

                    }
                });
    }

    // Setting Up One Time Navigation
    private void setupNavigation() {

        toolbar = findViewById(R.id.toolbar);

//        toolbar.setVisibility(View.INVISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.navigationView);

        navController = Navigation.findNavController(this, R.id.coach_nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout);

        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        name = headerView.findViewById(R.id.coach_drawer_name);
        email = headerView.findViewById(R.id.coach_drawer_email);
        profilePic = headerView.findViewById(R.id.coach_drawer_profile_pic);

    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(Navigation.findNavController(CoachMainActivity.this, R.id.coach_nav_host_fragment), drawerLayout);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        menuItem.setChecked(true);

        drawerLayout.closeDrawers();

        int id = menuItem.getItemId();



        switch (id) {

            case R.id.nav_home:
                System.out.println(currentPage+" "+R.id.coachHomeFragment+" "+navigationView.getId());
                if (currentPage != 2131361930){
                    navController.navigate(R.id.coachHomeFragment);
                    currentPage = R.id.coachHomeFragment;
                    break;
                }
                else {
                    break;
                }

            case R.id.nav_profile:
                System.out.println(currentPage+" "+R.id.coachProfile+" "+navController.getCurrentDestination().getAction(getTaskId()));
                if (currentPage != 2131361931){
                    navController.navigate(R.id.coachProfile);
                    currentPage = R.id.coachProfile;
                    break;
                }
                else {
                    break;
                }

            case R.id.nav_training_log:
                System.out.println(currentPage+" "+R.id.coachTrainingLog+" "+navController.getCurrentDestination().getAction(getTaskId()));
                if (currentPage != 2131361932){
                    navController.navigate(R.id.coachTrainingLog);
                    currentPage = R.id.coachTrainingLog;
                    break;
                }
                else {
                    break;
                }

            case R.id.coach_logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(CoachMainActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return true;

    }
}