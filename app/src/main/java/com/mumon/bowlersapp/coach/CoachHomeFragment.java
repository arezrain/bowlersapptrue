package com.mumon.bowlersapp.coach;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.Users;

public class CoachHomeFragment extends Fragment {

    CardView teamBtn, requestBtn, settingBtn;
    NavController navController;
    Toolbar toolbar;
    Window window;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();

    CardView teamList, requestList, trainingLog, setting;
    TextView name, email;
    Users users;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.coach_home_fragment, container, false);

        toolbar = getActivity().findViewById(R.id.toolbar);
        window = getActivity().getWindow();
        toolbar.setBackgroundColor(Color.argb(0,57,124,192));
        window.setStatusBarColor(Color.rgb(57,124,192));

        teamList = view.findViewById(R.id.coach_home_team_list_button);
        requestList = view.findViewById(R.id.coach_home_request_list_button);
        trainingLog = view.findViewById(R.id.coach_home_training_log_button);
        setting = view.findViewById(R.id.coach_home_setting_button);

        name = view.findViewById(R.id.coach_home_name);
        email = view.findViewById(R.id.coach_home_email);

        firebaseFirestore.document("users/"+user.getUid()+"/").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                name.setText(documentSnapshot.getString("name"));
                email.setText(user.getEmail());
            }
        });


        teamList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_coachHomeFragment_to_fragmentAthleteList);
            }
        });

        requestList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_coachHomeFragment_to_fragmentRequestList);
            }
        });

        trainingLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_coachHomeFragment_to_coachTrainingLog);

            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_coachHomeFragment_to_fragmentSettingCoach);
            }
        });

        return view;
    }

}
