package com.mumon.bowlersapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.Feedback;
import com.mumon.bowlersapp.model.LogModel;

public class NoteAdapter extends FirestoreRecyclerAdapter<LogModel, NoteAdapter.LogHolder> {
    private NoteAdapter.OnItemClickListener listener;
    private NoteAdapter.OnItemLongClickListener longClickListener;

    public NoteAdapter(@NonNull FirestoreRecyclerOptions<LogModel> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull NoteAdapter.LogHolder holder, int position, @NonNull LogModel model) {

        String dateText = model.getTime()+"   "+model.getDay()+"/"+model.getMonth()+"/"+model.getYear();
        holder.date.setText(dateText);
        holder.message.setText(model.getLogs());
        holder.title.setText(model.getTitle());
    }

    @NonNull
    @Override
    public NoteAdapter.LogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_content_athlete,
                parent, false);

        return new NoteAdapter.LogHolder(v);
    }



    public void deleteMessage(int position){
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    class LogHolder extends RecyclerView.ViewHolder {

        TextView date, message, title;

        public LogHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.athlete_note_title);
            date = itemView.findViewById(R.id.athlete_note_date);
            message = itemView.findViewById(R.id.athlete_note_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        longClickListener.onItemLongClick(getSnapshots().getSnapshot(position),position);
                    }

                    return false;
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public interface OnItemLongClickListener{
        void onItemLongClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(NoteAdapter.OnItemClickListener listener){
        this.listener = listener;
    }
    public void setOnItemLongClickListener(NoteAdapter.OnItemLongClickListener listener){
        this.longClickListener = listener;
    }
}
