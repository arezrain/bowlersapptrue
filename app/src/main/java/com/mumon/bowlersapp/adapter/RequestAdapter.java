package com.mumon.bowlersapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.RequestItem;

import java.util.ArrayList;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ExampleViewHolder> {

    private ArrayList<RequestItem> mRequestItems;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        public TextView name;
        public Button accept, decline;

        public ExampleViewHolder(View view, final OnItemClickListener listener){
            super(view);

            imageView = view.findViewById(R.id.request_profile_pic);
            name = view.findViewById(R.id.request_name);
            accept = view.findViewById(R.id.request_accept_btn);
            decline = view.findViewById(R.id.request_decline_btn);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });

        }
    }

    public RequestAdapter(ArrayList<RequestItem> requestItems){
        mRequestItems = requestItems;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_content, parent,false);
        ExampleViewHolder evh = new ExampleViewHolder(view, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        RequestItem currentItem = mRequestItems.get(position);

        holder.name.setText(currentItem.getName());
    }

    @Override
    public int getItemCount() {
        return mRequestItems.size();
    }
}
