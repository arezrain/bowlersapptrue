package com.mumon.bowlersapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.Users;

public class RequestAdapterNew extends FirestoreRecyclerAdapter<Users, RequestAdapterNew.RequestHolder> {
    private OnItemClickListener listener;
    private OnAcceptClickListener acceptClickListener;
    private OnDeclineClickListener declineClickListener;

    public RequestAdapterNew(@NonNull FirestoreRecyclerOptions<Users> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull RequestHolder holder, int position, @NonNull Users model) {

        holder.requestName.setText(model.getName());
        holder.requestEmail.setText(model.getEmail());
        if (model.getImage_uri()!=null){
            Glide.with(holder.profilePic.getContext()).load(model.getImage_uri()).centerCrop().into(holder.profilePic);
        }
    }

    @NonNull
    @Override
    public RequestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_content,
                parent, false);

        return new RequestHolder(v);
    }



    public void declineRequest(int position){
        getSnapshots().getSnapshot(position).getReference().update("request_key",null);
    }

    public void acceptRequest(int position, String key){

        getSnapshots().getSnapshot(position).getReference().update("team_key",key);
        getSnapshots().getSnapshot(position).getReference().update("request_key",null);
    }

    class RequestHolder extends RecyclerView.ViewHolder {

        TextView requestName, requestEmail;
        Button requestAccept, requestDecline;
        ImageView profilePic;

        public RequestHolder(@NonNull View itemView) {
            super(itemView);

            requestName = itemView.findViewById(R.id.request_name);
            requestAccept = itemView.findViewById(R.id.request_accept_btn);
            requestDecline = itemView.findViewById(R.id.request_decline_btn);
            requestEmail = itemView.findViewById(R.id.request_name_style);
            profilePic = itemView.findViewById(R.id.request_profile_pic);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

            requestAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        acceptClickListener.onAcceptClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

            requestDecline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        declineClickListener.onDeclineClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public interface OnAcceptClickListener{
        void onAcceptClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnAcceptClickListener(OnAcceptClickListener listener){
        this.acceptClickListener = listener;
    }

    public interface OnDeclineClickListener{
        void onDeclineClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnDeclineClickListener(OnDeclineClickListener listener){
        this.declineClickListener = listener;
    }
}
