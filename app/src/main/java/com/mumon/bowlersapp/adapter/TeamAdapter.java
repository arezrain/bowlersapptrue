package com.mumon.bowlersapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.TeamItem;

import java.util.ArrayList;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamViewHolder> {

    private ArrayList<TeamItem> mTeamItems;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public static class TeamViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        public TextView name;

        public TeamViewHolder(View view, final OnItemClickListener listener){
            super(view);

            imageView = view.findViewById(R.id.team_profile_pic);
            name = view.findViewById(R.id.team_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

    public TeamAdapter(ArrayList<TeamItem> teamItem){
        mTeamItems = teamItem;
    }

    @NonNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_list_content, parent,false);
        TeamViewHolder evh = new TeamViewHolder(view, mListener);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder holder, int position) {
        TeamItem currentItem = mTeamItems.get(position);

        holder.name.setText(currentItem.getName());
    }

    @Override
    public int getItemCount() {
        return mTeamItems.size();
    }
}
