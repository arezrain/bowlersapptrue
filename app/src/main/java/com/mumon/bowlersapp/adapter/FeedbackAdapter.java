package com.mumon.bowlersapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.Feedback;

public class FeedbackAdapter extends FirestoreRecyclerAdapter<Feedback, FeedbackAdapter.FeedbackHolder> {
    private FeedbackAdapter.OnItemClickListener listener;
    private FeedbackAdapter.OnItemLongClickListener longClickListener;

    public FeedbackAdapter(@NonNull FirestoreRecyclerOptions<Feedback> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull FeedbackAdapter.FeedbackHolder holder, int position, @NonNull Feedback model) {

        String dateText = model.getTime()+"   "+model.getDay()+"/"+model.getMonth()+"/"+model.getYear();
        holder.date.setText(dateText);
        holder.message.setText(model.getMessage());
    }

    @NonNull
    @Override
    public FeedbackAdapter.FeedbackHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_item,
                parent, false);

        return new FeedbackAdapter.FeedbackHolder(v);
    }



    public void deleteMessage(int position){
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    class FeedbackHolder extends RecyclerView.ViewHolder {

        TextView date, message;

        public FeedbackHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.feedback_date);
            message = itemView.findViewById(R.id.feedback_message);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        longClickListener.onItemLongClick(getSnapshots().getSnapshot(position),position);
                    }

                    return false;
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public interface OnItemLongClickListener{
        void onItemLongClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(FeedbackAdapter.OnItemClickListener listener){
        this.listener = listener;
    }
    public void setOnItemLongClickListener(FeedbackAdapter.OnItemLongClickListener listener){
        this.longClickListener = listener;
    }
}
