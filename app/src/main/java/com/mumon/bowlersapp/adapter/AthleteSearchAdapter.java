package com.mumon.bowlersapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mumon.bowlersapp.R;
import com.mumon.bowlersapp.model.Users;

public class AthleteSearchAdapter extends FirestoreRecyclerAdapter<Users, AthleteSearchAdapter.SearchHolder> {

    private OnItemClickListener listener;

    public AthleteSearchAdapter(@NonNull FirestoreRecyclerOptions<Users> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull AthleteSearchAdapter.SearchHolder holder, int position, @NonNull Users model) {

        holder.searchName.setText(model.getName());
    }

    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.athlete_search_content,
                parent, false);

        return new SearchHolder(v);
    }

    class SearchHolder extends RecyclerView.ViewHolder {

        TextView searchName;

        public SearchHolder(@NonNull View itemView) {
            super(itemView);

            searchName = itemView.findViewById(R.id.search_athlete_name);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null){
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });

        }
    }

    public interface OnItemClickListener{
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
}
